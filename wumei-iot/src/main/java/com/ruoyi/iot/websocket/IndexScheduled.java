/*
package com.ruoyi.iot.websocket;


import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.iot.axure.domain.Result;
import com.ruoyi.iot.axure.dto.HydrologyListDto;
import com.ruoyi.iot.domain.Device;
import com.ruoyi.iot.mqtt.EmqxClient;
import com.ruoyi.iot.service.IDeviceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class IndexScheduled {

    @Autowired
    private IDeviceService iDeviceService;

    //1、实时向前端推送行车位置信息
    //@Scheduled(cron = "0/3 * * * * ?")
    */
/*public void selectDrivingPosition() {
        //Result result = iDeviceService.selectDrivingPosition();
        DrivingPositionDto positionDto = iDeviceService.selectDrivingPosition();
        System.out.println(positionDto);
        AjaxResult message = AjaxResult.success(positionDto);
        WebSocket.sendMessage(message, "11");
    }*//*


    //定义一个方法。用于向前端h5展示水库列表及其点位数据
    */
/*@Scheduled(cron = "0/3 * * * * ?")
    public void selectHydrologyList() {
        //查询水库列表信息
        List<Device> devices = iDeviceService.selectDeviceList(null);
        ArrayList<HydrologyListDto> arrayList = new ArrayList<>();
        if (devices != null && StringUtils.isEmpty(devices)) {
            for (Device device : devices) {
                HydrologyListDto listDto = new HydrologyListDto();
                listDto.setId(device.getDeviceId());
                listDto.setHydrologyNum(device.getSerialNumber());
                listDto.setDeviceName(device.getDeviceName());
                //获取物模型值
                String thingsModelValue = device.getThingsModelValue();

                if (thingsModelValue.length() > 0 && !StringUtils.isEmpty(thingsModelValue)) {
                    thingsModelValue = thingsModelValue.replaceAll(" ", "").replaceAll("]", "").replaceAll("\\[", "");
                }
                String[] split = thingsModelValue.split("},");
                if (split.length > 0 && !StringUtils.isEmpty(split)) {
                    for (String s1 : split) {
                        if (s1.contains("rainfall")) {
                            if (!s1.endsWith("}")) {
                                s1 = s1 + "}";
                            }
                            JSONObject jsonObject = JSONObject.parseObject(s1);
                            String shadow = jsonObject.get("shadow").toString();
                            if ("".equals(shadow) || StringUtils.isEmpty(shadow)) {
                                shadow = "null";
                            }
                            listDto.setRainfall(Double.valueOf(shadow));
                        }
                        if (s1.contains("waterlevel")) {
                            if (!s1.endsWith("}")) {
                                s1 = s1 + "}";
                            }
                            JSONObject jsonObject = JSONObject.parseObject(s1);
                            String shadow = jsonObject.get("shadow").toString();
                            if ("".equals(shadow) || StringUtils.isEmpty(shadow)) {
                                shadow = "null";
                            }
                            listDto.setWaterLevel(Double.valueOf(shadow));
                        }
                    }
                }
                arrayList.add(listDto);
            }
        }
        AjaxResult success = AjaxResult.success(arrayList);
        WebSocket.sendMessage(success, "01");
    }
*//*

}
*/
