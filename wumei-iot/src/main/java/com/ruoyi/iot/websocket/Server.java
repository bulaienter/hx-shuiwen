package com.ruoyi.iot.websocket;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * socket接收端，模拟Tcp发送数据，在这里接收
 */
public class Server {

    public static void server() {
        try {
            ServerSocket serverSocket = new ServerSocket(1100);
            System.out.println("----------------服务端执行，開始监听请求----------------");

            Socket socket = serverSocket.accept();//開始监听
            InputStream inputStream = socket.getInputStream();
            //获取请求内容
            String info;
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            while ((info = bufferedReader.readLine()) != null) {
                System.out.println(info);
            }

            OutputStream put = socket.getOutputStream();
            String putText = "接收成功";       //接收成功
            put.write(putText.getBytes());    //将输出流信息以二进制的形式进行写入
            //关闭资源
            socket.shutdownInput();
            bufferedReader.close();
            inputStream.close();
            socket.close();
            serverSocket.close();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public static void main(String[] args) {
        while (true) {
            server();
        }
    }
}
