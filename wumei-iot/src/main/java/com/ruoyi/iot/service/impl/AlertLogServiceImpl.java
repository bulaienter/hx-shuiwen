package com.ruoyi.iot.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.iot.axure.domain.DeviceAlertDto;
import com.ruoyi.iot.axure.dto.DeviceAlertDtoResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.iot.mapper.AlertLogMapper;
import com.ruoyi.iot.domain.AlertLog;
import com.ruoyi.iot.service.IAlertLogService;

/**
 * 设备告警Service业务层处理
 *
 * @author kerwincui
 * @date 2022-01-13
 */
@Service
public class AlertLogServiceImpl implements IAlertLogService {
    @Autowired
    private AlertLogMapper alertLogMapper;

    /**
     * 查询设备告警
     *
     * @param alertLogId 设备告警主键
     * @return 设备告警
     */
    @Override
    public AlertLog selectAlertLogByAlertLogId(Long alertLogId) {
        return alertLogMapper.selectAlertLogByAlertLogId(alertLogId);
    }

    /**
     * 查询设备告警列表
     *
     * @param alertLog 设备告警
     * @return 设备告警
     */
    @Override
    public List<AlertLog> selectAlertLogList(AlertLog alertLog) {
        return alertLogMapper.selectAlertLogList(alertLog);
    }

    /**
     * 新增设备告警
     *
     * @param alertLog 设备告警
     * @return 结果
     */
    @Override
    public int insertAlertLog(AlertLog alertLog) {
        alertLog.setCreateTime(DateUtils.getNowDate());
        return alertLogMapper.insertAlertLog(alertLog);
    }

    /**
     * 修改设备告警
     *
     * @param alertLog 设备告警
     * @return 结果
     */
    @Override
    public int updateAlertLog(AlertLog alertLog) {
        alertLog.setUpdateTime(DateUtils.getNowDate());
        return alertLogMapper.updateAlertLog(alertLog);
    }

    /**
     * 批量删除设备告警
     *
     * @param alertLogIds 需要删除的设备告警主键
     * @return 结果
     */
    @Override
    public int deleteAlertLogByAlertLogIds(Long[] alertLogIds) {
        return alertLogMapper.deleteAlertLogByAlertLogIds(alertLogIds);
    }

    /**
     * 删除设备告警信息
     *
     * @param alertLogId 设备告警主键
     * @return 结果
     */
    @Override
    public int deleteAlertLogByAlertLogId(Long alertLogId) {
        return alertLogMapper.deleteAlertLogByAlertLogId(alertLogId);
    }

    @Override
    public DeviceAlertDtoResult getDeviceAlertList() {
        List<DeviceAlertDto> list = alertLogMapper.getDeviceAlertList();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //创建实体类，用于封装报警设备返回的参数
        DeviceAlertDtoResult alertDtoResult = new DeviceAlertDtoResult();
        //严重报警数
        int criticalAlarmNum = 0;
        //一般报警数
        int generalAlarmNum = 0;
        if (list.size() > 0 && !StringUtils.isEmpty(list)) {
            for (DeviceAlertDto deviceAlertDto : list) {
                Long alertLevel = deviceAlertDto.getAlertLevel();
                if (alertLevel == 3) {
                    criticalAlarmNum++;
                } else {
                    generalAlarmNum++;
                }
                //获取设备告警日期
                Date createTime = deviceAlertDto.getCreateTime();
                String format = dateFormat.format(createTime);
                deviceAlertDto.setAlertTime(format);
                deviceAlertDto.setCreateTime(null);
            }
        }
        alertDtoResult.setCriticalAlarmNum(criticalAlarmNum);
        alertDtoResult.setGeneralAlarmNum(generalAlarmNum);
        alertDtoResult.setAlertDtoList(list);
        return alertDtoResult;
    }
}
