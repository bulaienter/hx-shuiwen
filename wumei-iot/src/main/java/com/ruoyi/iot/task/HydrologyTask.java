package com.ruoyi.iot.task;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.iot.axure.dto.HydrologyListDto;
import com.ruoyi.iot.axure.thread.HydrologyNewThread;
import com.ruoyi.iot.axure.thread.HydrologyThread;
import com.ruoyi.iot.axure.utils.TCPUtil;
import com.ruoyi.iot.domain.Device;
import com.ruoyi.iot.service.IDeviceService;
import com.ruoyi.iot.websocket.WebSocket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.*;

import static com.ruoyi.iot.axure.utils.CRCUtil.calculateCRC;

/**
 * 水库定时任务
 */
@Component("HydrologyTask")
public class HydrologyTask {
    //ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(5, 10, 30, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(50), Executors.defaultThreadFactory(), new ThreadPoolExecutor.CallerRunsPolicy());
    //流水号
    private int liushuihao = 1;
    //1、起始符
    private String qs = "7E7E";
    //2、中心站地址
    private String zxz = "00";
    //3、功能码
    private String gnm = "33";
    //4、密码
    private String mima = "0000";
    //5、上行下行标志长度
    private String cd = "001D";
    //6、起始符
    private String qsf = "02";
    //7、结束符
    private String jsf = "03";
    //8、分类码
    private String flm = "48";
    //9、更新时间格式化
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yy:MM:dd HH:mm");
    @Autowired
    private IDeviceService iDeviceService;

    //1、websocket 向前端推送水库列表点位信息
    public void selectHydrologyList() {
        //IDeviceService iDeviceService = GetBeanUtils.getBean(IDeviceService.class);
        //查询水库列表信息
        List<Device> devices = iDeviceService.selectDeviceLists();
        ArrayList<HydrologyListDto> arrayList = new ArrayList<>();
        if (devices != null && !StringUtils.isEmpty(devices)) {
            for (Device device : devices) {
                HydrologyListDto listDto = new HydrologyListDto();
                listDto.setDeviceId(device.getDeviceId());
                listDto.setSerialNumber(device.getSerialNumber());
                listDto.setDeviceName(device.getDeviceName());
                //获取物模型值
                String thingsModelValue = device.getThingsModelValue();

                if (thingsModelValue.length() > 0 && !StringUtils.isEmpty(thingsModelValue)) {
                    thingsModelValue = thingsModelValue.replaceAll(" ", "").replaceAll("]", "").replaceAll("\\[", "");
                }
                String[] split = thingsModelValue.split("},");
                if (split.length > 0 && !StringUtils.isEmpty(split)) {
                    for (String s1 : split) {
                        if (s1.contains("rainfall")) {
                            if (!s1.endsWith("}")) {
                                s1 = s1 + "}";
                            }
                            JSONObject jsonObject = JSONObject.parseObject(s1);
                            String shadow = jsonObject.get("shadow").toString();
                            if ("".equals(shadow) || StringUtils.isEmpty(shadow)) {
                                shadow = "0";
                            }
                            listDto.setRainfall(Double.valueOf(shadow));
                        }
                        if (s1.contains("waterlevel")) {
                            if (!s1.endsWith("}")) {
                                s1 = s1 + "}";
                            }
                            JSONObject jsonObject = JSONObject.parseObject(s1);
                            String shadow = jsonObject.get("shadow").toString();
                            if ("".equals(shadow) || StringUtils.isEmpty(shadow)) {
                                shadow = "0";
                            }
                            listDto.setWaterLevel(Double.valueOf(shadow));
                        }
                    }
                }
                arrayList.add(listDto);
            }
        }
        AjaxResult success = AjaxResult.success(arrayList);
        WebSocket.sendMessage(success, "01");
    }

    //2、向水文中心站定时推送水库水位数据
    /*public void selectHydrologyUpper() {
        //线程池优化
        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(4, 8, 60, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(50), Executors.defaultThreadFactory(), new ThreadPoolExecutor.CallerRunsPolicy());

        String sendTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yy:MM:dd HH:mm:ss")).replaceAll(" ", "").replaceAll(":", "");
        String substring = sendTime.substring(0, 10);
        ArrayList<String> list = new ArrayList<>();
        List<Device> devices = iDeviceService.selectDeviceLists();
        if (devices.size() > 0 && !StringUtils.isEmpty(devices)) {
            //查到每一个水库设备信息，根据需求组织成对应的报文格式，通过socket发送给指定的ip
            for (Device device : devices) {
                //5、流水号
                String hexLiuShuiHao = String.format("%04X", liushuihao);
                liushuihao++;
                if (liushuihao >= 65535) {
                    liushuihao = 1;
                }
                //获取设备编号
                String serialNumber = device.getSerialNumber();
                //定义两个变量，用于接收水库水位 以及 水库降雨量
                String waterLevel = "";
                String rainfall = "";
                //获取物模型值
                String thingsModelValue = device.getThingsModelValue();
                if (thingsModelValue.length() > 0 && !StringUtils.isEmpty(thingsModelValue)) {
                    thingsModelValue = thingsModelValue.replaceAll(" ", "").replaceAll("]", "").replaceAll("\\[", "");
                }
                String[] split = thingsModelValue.split("},");
                if (split.length > 0 && !StringUtils.isEmpty(split)) {
                    for (String s1 : split) {
                        if (s1.contains("rainfall")) {
                            if (!s1.endsWith("}")) {
                                s1 = s1 + "}";
                            }
                            JSONObject jsonObject = JSONObject.parseObject(s1);
                            String shadow = jsonObject.get("shadow").toString();
                            if ("".equals(shadow) || StringUtils.isEmpty(shadow)) {
                                shadow = "0";
                            }
                            rainfall = shadow;
                        }
                        if (s1.contains("waterlevel")) {
                            if (!s1.endsWith("}")) {
                                s1 = s1 + "}";
                            }
                            JSONObject jsonObject = JSONObject.parseObject(s1);
                            String shadow = jsonObject.get("shadow").toString();
                            if ("".equals(shadow) || StringUtils.isEmpty(shadow)) {
                                shadow = "0";
                            }
                            waterLevel = shadow;
                        }
                    }
                }
                //2、降雨量  可以在前台传小数 保留一位小数 单位mm
                double v1 = Double.parseDouble(rainfall);
                DecimalFormat df1 = new DecimalFormat("0.0");
                String format1 = df1.format(v1);
                String replace = format1.replace(".", "");
                String jyl = "2019" + String.valueOf(Integer.parseInt(replace) + 100000).replaceFirst("1", "0");
                //3、水位  可以在前台传小数三位小数  单位m
                double v = Double.parseDouble(waterLevel);
                DecimalFormat df2 = new DecimalFormat("0.000");
                String formattedNum = df2.format(v);
                String s1 = formattedNum.replace(".", "");
                String sw = "3923" + String.valueOf(Integer.parseInt(s1) + 10000000).replaceFirst("1", "0");
                //7、拼接报文
                //String json = qs + zxz + serialNumber + mima + gnm + cd + qsf + hexLiuShuiHao + sendTime + "F1F1" + serialNumber + flm + "F0F0" + substring + jyl + sw + jsf;
                //定时任务只发水位不发降雨量
                String json = qs + zxz + serialNumber + mima + gnm + cd + qsf + hexLiuShuiHao + sendTime + "F1F1" + serialNumber + flm + "F0F0" + substring + sw + jsf;
                //8、报文CRC校验
                String cRC = calculateCRC(json);
                json = json + cRC;
                System.out.println("发送的报文是：" + json);
                list.add(json);

                //发送报文
                //TCPUtil.sendMessage(json);
            }
            //发送报文
            //TCPUtil.sendMessages(list);
        }
        if (list.size() > 0 && !StringUtils.isEmpty(list)) {
            for (String json : list) {
                //TCPUtil.sendMessage(json);
                //使用线程池发送数据
                poolExecutor.execute(new HydrologyThread(json));
            }
        }
        poolExecutor.shutdown();
    }*/

    //2.2 向中心站定时推送水库水位数据，需要根据每个水库记录的中心站ip和端口进行上传 2024.01.24
    public void selectHydrologyUpper() {
        //线程池优化
        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(5, 10, 30, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(50), Executors.defaultThreadFactory(), new ThreadPoolExecutor.CallerRunsPolicy());
        String sendTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yy:MM:dd HH:mm:ss")).replaceAll(" ", "").replaceAll(":", "");
        String substring = sendTime.substring(0, 10);
        List<Device> devices = iDeviceService.selectDeviceLists();
        if (devices.size() > 0 && !StringUtils.isEmpty(devices)) {
            //查到每一个水库设备信息，根据需求组织成对应的报文格式，通过socket发送给指定的ip
            for (Device device : devices) {
                //5、流水号
                String hexLiuShuiHao = String.format("%04X", liushuihao);
                liushuihao++;
                if (liushuihao >= 65535) {
                    liushuihao = 1;
                }
                //获取设备编号
                String serialNumber = device.getSerialNumber();
                //定义三个变量，用于保存水库水位，对应中心站的ip和端口
                String waterLevel = "";
                String addressIp = "";
                String addressPort = "";
                //获取物模型值
                String thingsModelValue = device.getThingsModelValue();
                if (thingsModelValue.length() > 0 && !StringUtils.isEmpty(thingsModelValue)) {
                    thingsModelValue = thingsModelValue.replaceAll(" ", "").replaceAll("]", "").replaceAll("\\[", "");
                    String[] split = thingsModelValue.split("},");
                    if (split.length > 0 && !StringUtils.isEmpty(split)) {
                        for (String s1 : split) {
                            if (s1.contains("waterlevel")) {
                                if (!s1.endsWith("}")) {
                                    s1 = s1 + "}";
                                }
                                JSONObject jsonObject = JSONObject.parseObject(s1);
                                String shadow = jsonObject.get("shadow").toString();
                                if ("".equals(shadow) || StringUtils.isEmpty(shadow)) {
                                    shadow = "0";
                                }
                                waterLevel = shadow;
                            }
                            //2024.01.25 获取中心站ip
                            if (s1.contains("provinceIp")) {
                                if (!s1.endsWith("}")) {
                                    s1 = s1 + "}";
                                }
                                JSONObject jsonObject = JSONObject.parseObject(s1);
                                String shadow = jsonObject.get("shadow").toString();
                                if ("".equals(shadow) || StringUtils.isEmpty(shadow)) {
                                    shadow = "0";
                                }
                                addressIp = shadow;
                            }
                            if (s1.contains("municipalIP")) {
                                if (!s1.endsWith("}")) {
                                    s1 = s1 + "}";
                                }
                                JSONObject jsonObject = JSONObject.parseObject(s1);
                                String shadow = jsonObject.get("shadow").toString();
                                if ("".equals(shadow) || StringUtils.isEmpty(shadow)) {
                                    shadow = "0";
                                }
                                addressIp = shadow;
                            }
                            //2024.01.25 获取中心站端口
                            if (s1.contains("provincePort")) {
                                if (!s1.endsWith("}")) {
                                    s1 = s1 + "}";
                                }
                                JSONObject jsonObject = JSONObject.parseObject(s1);
                                String shadow = jsonObject.get("shadow").toString();
                                if ("".equals(shadow) || StringUtils.isEmpty(shadow) || "null".equals(shadow)) {
                                    shadow = "0";
                                }
                                addressPort = shadow;
                            }
                            if (s1.contains("municipalPort")) {
                                if (!s1.endsWith("}")) {
                                    s1 = s1 + "}";
                                }
                                JSONObject jsonObject = JSONObject.parseObject(s1);
                                String shadow = jsonObject.get("shadow").toString();
                                if ("".equals(shadow) || StringUtils.isEmpty(shadow) || "null".equals(shadow)) {
                                    shadow = "0";
                                }
                                addressPort = shadow;
                            }
                        }
                    }
                }
                //3、水位
                double v = Double.parseDouble(waterLevel);
                DecimalFormat df2 = new DecimalFormat("0.000");
                String formattedNum = df2.format(v);
                String s1 = formattedNum.replace(".", "");
                String sw = "3923" + String.valueOf(Integer.parseInt(s1) + 10000000).replaceFirst("1", "0");
                //7、拼接报文
                //定时任务只发水位不发降雨量
                String json = qs + zxz + serialNumber + mima + gnm + cd + qsf + hexLiuShuiHao + sendTime + "F1F1" + serialNumber + flm + "F0F0" + substring + sw + jsf;
                //8、报文CRC校验
                String cRC = calculateCRC(json);
                json = json + cRC;
                //System.out.println("发送的报文是：" + json);
                //9、中心站端口号
                int centralStationPort = Integer.parseInt(addressPort);
                //发送报文
                if (addressIp != "" && !StringUtils.isEmpty(addressIp) && addressPort != "" && !StringUtils.isEmpty(addressPort)) {
                    poolExecutor.execute(new HydrologyNewThread(addressIp, centralStationPort, json));
                }
            }
        }
        poolExecutor.shutdown();
    }

    //3、后台测试定时任务，测试版本
    //@Scheduled(cron = "0 0/5 * * * ? ")
    public void hydrologyTask() {
        System.out.println("定时任务执行");
        //创建一个线程池
        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(4, 8, 60, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(50), Executors.defaultThreadFactory(), new ThreadPoolExecutor.CallerRunsPolicy());

        String sendTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yy:MM:dd HH:mm:ss")).replaceAll(" ", "").replaceAll(":", "");
        String substring = sendTime.substring(0, 10);
        ArrayList<String> list = new ArrayList<>();
        List<Device> devices = iDeviceService.selectDeviceLists();
        if (devices.size() > 0 && !StringUtils.isEmpty(devices)) {
            for (Device device : devices) {
                String hexLiuShuiHao = String.format("%04X", liushuihao);
                liushuihao++;
                if (liushuihao >= 65535) {
                    liushuihao = 1;
                }
                String serialNumber = device.getSerialNumber();
                String waterLevel = "";
                String rainfall = "";
                String thingsModelValue = device.getThingsModelValue();
                if (thingsModelValue.length() > 0 && !StringUtils.isEmpty(thingsModelValue)) {
                    thingsModelValue = thingsModelValue.replaceAll(" ", "").replaceAll("]", "").replaceAll("\\[", "");
                }
                String[] split = thingsModelValue.split("},");
                if (split.length > 0 && !StringUtils.isEmpty(split)) {
                    for (String s1 : split) {
                        if (s1.contains("rainfall")) {
                            if (!s1.endsWith("}")) {
                                s1 = s1 + "}";
                            }
                            JSONObject jsonObject = JSONObject.parseObject(s1);
                            String shadow = jsonObject.get("shadow").toString();
                            if ("".equals(shadow) || StringUtils.isEmpty(shadow)) {
                                shadow = "0";
                            }
                            rainfall = shadow;
                        }
                        if (s1.contains("waterlevel")) {
                            if (!s1.endsWith("}")) {
                                s1 = s1 + "}";
                            }
                            JSONObject jsonObject = JSONObject.parseObject(s1);
                            String shadow = jsonObject.get("shadow").toString();
                            if ("".equals(shadow) || StringUtils.isEmpty(shadow)) {
                                shadow = "0";
                            }
                            waterLevel = shadow;
                        }
                    }
                }
                double v1 = Double.parseDouble(rainfall);
                DecimalFormat df1 = new DecimalFormat("0.0");
                String format1 = df1.format(v1);
                String replace = format1.replace(".", "");
                String jyl = "2019" + String.valueOf(Integer.parseInt(replace) + 100000).replaceFirst("1", "0");
                double v = Double.parseDouble(waterLevel);
                DecimalFormat df2 = new DecimalFormat("0.000");
                String formattedNum = df2.format(v);
                String s1 = formattedNum.replace(".", "");
                String sw = "3923" + String.valueOf(Integer.parseInt(s1) + 10000000).replaceFirst("1", "0");
                //定时任务只发水位不发降雨量
                String json = qs + zxz + serialNumber + mima + gnm + cd + qsf + hexLiuShuiHao + sendTime + "F1F1" + serialNumber + flm + "F0F0" + substring + sw + jsf;
                //8、报文CRC校验
                String cRC = calculateCRC(json);
                json = json + cRC;
                System.out.println("发送的报文是：" + json);
                list.add(json);
                //发送报文
                //TCPUtil.sendMessage(json);
            }
            //发送报文
            //TCPUtil.sendMessages(list);
        }
        if (list.size() > 0 && !StringUtils.isEmpty(list)) {
            for (String json : list) {
                //TCPUtil.sendMessage(json);
                //使用线程池发送数据
                poolExecutor.execute(new HydrologyThread(json));
            }
        }
        poolExecutor.shutdown();
    }

    //4、后台测试定时任务，测试版本，需要根据水库列表中获取中心站ip和端口
    //@Scheduled(cron = "0 0/2 * * * ? ")
    public void hydrologyTasks() {
        //线程池优化
        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(6, 12, 30, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(100), Executors.defaultThreadFactory(), new ThreadPoolExecutor.CallerRunsPolicy());
        String sendTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yy:MM:dd HH:mm:ss")).replaceAll(" ", "").replaceAll(":", "");
        String substring = sendTime.substring(0, 10);
        List<Device> devices = iDeviceService.selectDeviceLists();
        if (devices.size() > 0 && !StringUtils.isEmpty(devices)) {
            //查到每一个水库设备信息，根据需求组织成对应的报文格式，通过socket发送给指定的ip
            for (Device device : devices) {
                //5、流水号
                String hexLiuShuiHao = String.format("%04X", liushuihao);
                liushuihao++;
                if (liushuihao >= 65535) {
                    liushuihao = 1;
                }
                //获取设备编号
                String serialNumber = device.getSerialNumber();
                //定义三个变量，用于保存水库水位，对应中心站的ip和端口
                String waterLevel = "";
                String addressIp = "";
                String addressPort = "";
                //获取物模型值
                String thingsModelValue = device.getThingsModelValue();
                if (thingsModelValue.length() > 0 && !StringUtils.isEmpty(thingsModelValue)) {
                    thingsModelValue = thingsModelValue.replaceAll(" ", "").replaceAll("]", "").replaceAll("\\[", "");
                    String[] split = thingsModelValue.split("},");
                    if (split.length > 0 && !StringUtils.isEmpty(split)) {
                        for (String s1 : split) {
                            if (s1.contains("waterlevel")) {
                                if (!s1.endsWith("}")) {
                                    s1 = s1 + "}";
                                }
                                JSONObject jsonObject = JSONObject.parseObject(s1);
                                String shadow = jsonObject.get("shadow").toString();
                                if ("".equals(shadow) || StringUtils.isEmpty(shadow)) {
                                    shadow = "0";
                                }
                                waterLevel = shadow;
                            }
                            //2024.01.25 获取中心站ip
                            if (s1.contains("provinceIp")) {
                                if (!s1.endsWith("}")) {
                                    s1 = s1 + "}";
                                }
                                JSONObject jsonObject = JSONObject.parseObject(s1);
                                String shadow = jsonObject.get("shadow").toString();
                                if ("".equals(shadow) || StringUtils.isEmpty(shadow)) {
                                    shadow = "0";
                                }
                                addressIp = shadow;
                            }
                            if (s1.contains("municipalIP")) {
                                if (!s1.endsWith("}")) {
                                    s1 = s1 + "}";
                                }
                                JSONObject jsonObject = JSONObject.parseObject(s1);
                                String shadow = jsonObject.get("shadow").toString();
                                if ("".equals(shadow) || StringUtils.isEmpty(shadow)) {
                                    shadow = "0";
                                }
                                addressIp = shadow;
                            }
                            //2024.01.25 获取中心站端口
                            if (s1.contains("provincePort")) {
                                if (!s1.endsWith("}")) {
                                    s1 = s1 + "}";
                                }
                                JSONObject jsonObject = JSONObject.parseObject(s1);
                                String shadow = jsonObject.get("shadow").toString();
                                if ("".equals(shadow) || StringUtils.isEmpty(shadow) || "null".equals(shadow)) {
                                    shadow = "0";
                                }
                                addressPort = shadow;
                            }
                            if (s1.contains("municipalPort")) {
                                if (!s1.endsWith("}")) {
                                    s1 = s1 + "}";
                                }
                                JSONObject jsonObject = JSONObject.parseObject(s1);
                                String shadow = jsonObject.get("shadow").toString();
                                if ("".equals(shadow) || StringUtils.isEmpty(shadow) || "null".equals(shadow)) {
                                    shadow = "0";
                                }
                                addressPort = shadow;
                            }
                        }
                    }
                }
                //3、水位  可以在前台传小数三位小数  单位m
                double v = Double.parseDouble(waterLevel);
                DecimalFormat df2 = new DecimalFormat("0.000");
                String formattedNum = df2.format(v);
                String s1 = formattedNum.replace(".", "");
                String sw = "3923" + String.valueOf(Integer.parseInt(s1) + 10000000).replaceFirst("1", "0");
                //7、拼接报文
                //定时任务只发水位不发降雨量
                String json = qs + zxz + serialNumber + mima + gnm + cd + qsf + hexLiuShuiHao + sendTime + "F1F1" + serialNumber + flm + "F0F0" + substring + sw + jsf;
                //8、报文CRC校验
                String cRC = calculateCRC(json);
                json = json + cRC;
                //System.out.println("发送的报文是：" + json);
                //9、中心站端口号
                int centralStationPort = Integer.parseInt(addressPort);
                //发送报文
                if (addressIp != "" && !StringUtils.isEmpty(addressIp) && addressPort != "" && !StringUtils.isEmpty(addressPort)) {
                    poolExecutor.execute(new HydrologyNewThread(addressIp, centralStationPort, json));
                }
            }
        }
        poolExecutor.shutdown();
    }
}




