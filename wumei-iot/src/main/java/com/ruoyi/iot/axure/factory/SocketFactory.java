package com.ruoyi.iot.axure.factory;

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

import java.net.Socket;

public class SocketFactory extends BasePooledObjectFactory<Socket> {
    private String serverIp;
    private int serverPort;

    public SocketFactory(String serverIp, int serverPort) {
        this.serverIp = serverIp;
        this.serverPort = serverPort;
    }

    @Override
    public Socket create() throws Exception {
        return new Socket(serverIp, serverPort);
    }

    @Override
    public PooledObject<Socket> wrap(Socket socket) {
        return new DefaultPooledObject<>(socket);
    }
}
