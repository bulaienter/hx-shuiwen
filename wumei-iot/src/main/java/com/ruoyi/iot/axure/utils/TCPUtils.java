package com.ruoyi.iot.axure.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.List;

public class TCPUtils {
    private static String serverAddress = "112.30.214.13"; // 中心站的IP地址
    private static int serverPort = 6000; // 中心站的端口号
    static Socket socket;
    static {
        try {
            socket = new Socket(serverAddress,serverPort);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //单独发送
    public static void sendMessage(String resultJson) {
        try  {
            System.out.println("成功连接中心站。。。");
            OutputStream outputStream = socket.getOutputStream();
            byte[] dataBytes = hexStringToByteArray(resultJson);
            outputStream.write(dataBytes);
            outputStream.flush();
            // 获取输入流，用于接收中心站的确认响应
            InputStream inputStream = socket.getInputStream();
            byte[] buffer = new byte[1024];
            int length = inputStream.read(buffer);
            String response = new String(buffer, 0, length);
            System.out.println("中心站的确认响应：" + response);
            inputStream.close();
            outputStream.close();
            socket.close();
            System.out.println("水文报文已发送至中心站");
        } catch (Exception e) {
            // 连接失败，输出错误信息
            System.out.println("无法连接中心站：" + e.getMessage());
            // 进行重试或其他处理
            sendMessage(resultJson);
        }
    }

    //批量发送
    public static void sendMessages(List<String> resultJson) {
        try{
            System.out.println("成功连接中心站。。。");
            OutputStream outputStream = socket.getOutputStream();
            for (String json : resultJson) {
                byte[] dataBytes = hexStringToByteArray(json);
                outputStream.write(dataBytes);
                outputStream.flush();
                System.out.println("批量发送的报文为：" + json);
            }
            // 获取输入流，用于接收中心站的确认响应
            InputStream inputStream = socket.getInputStream();
            byte[] buffer = new byte[1024];
            int length = inputStream.read(buffer);
            String response = new String(buffer, 0, length);
            System.out.println("中心站的确认响应：" + response);
            inputStream.close();
            outputStream.close();
            socket.close();
            System.out.println("水文报文已发送至中心站");
        } catch (Exception e) {
            System.out.println("无法连接中心站：" + e.getMessage());
            // 进行重试
            sendMessages(resultJson);
        }
    }


    // 将十六进制字符串转换为字节数组
    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
}
