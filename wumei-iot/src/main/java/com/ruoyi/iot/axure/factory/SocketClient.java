package com.ruoyi.iot.axure.factory;

import org.apache.commons.pool2.impl.GenericObjectPool;

import java.net.Socket;

public class SocketClient {
    private static final String SERVER_IP = "127.0.0.1";
    private static final int SERVER_PORT = 8080;
    private static final int MAX_CONNECTIONS = 10;

    private static GenericObjectPool<Socket> connectionPool;
}
