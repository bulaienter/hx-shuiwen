package com.ruoyi.iot.axure.factory;

import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class TCPUtills {

    private static final String SERVER_IP = "112.30.214.13";
    private static final int SERVER_PORT = 6000;
    private static final int MAX_CONNECTIONS = 50;
    private static GenericObjectPool<Socket> connectionPool;

    //批量发送
    public static void sendMessage(List<String> resultJson) {
        // 配置连接池
        GenericObjectPoolConfig<Socket> poolConfig = new GenericObjectPoolConfig<>();
        poolConfig.setMaxTotal(MAX_CONNECTIONS);
        connectionPool = new GenericObjectPool<>(new SocketFactory(SERVER_IP, SERVER_PORT), poolConfig);
        for (String json : resultJson) {
            Socket socket = null;
            OutputStream outputStream = null;
            InputStream inputStream = null;
            try {
                // 从连接池获取连接
                socket = connectionPool.borrowObject();
                System.out.println("成功连接中心站。。。");
                outputStream = socket.getOutputStream();
                inputStream = socket.getInputStream();
                byte[] dataBytes = hexStringToByteArray(json);
                outputStream.write(dataBytes);
                outputStream.flush();
                // 获取输入流，用于接收中心站的确认响应
                byte[] buffer = new byte[1024];
                int length = inputStream.read(buffer);
                String response = new String(buffer, 0, length);
                //处理中心站的确认响应
                System.out.println("中心站的确认响应：" + response);
                // 关闭输入流、输出流和Socket连接
                System.out.println("水文报文已发送至中心站");
            } catch (Exception e) {
                // 连接失败，输出错误信息
                System.out.println("无法连接中心站：" + e.getMessage());
                // 进行重试或其他处理
                ArrayList<String> list = new ArrayList<>();
                list.add(json);
                sendMessage(list);
            } finally {
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if (socket != null) {
                    // 将连接放回连接池
                    connectionPool.returnObject(socket);
                }
            }
        }
    }
    // 将十六进制字符串转换为字节数组
    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
}
