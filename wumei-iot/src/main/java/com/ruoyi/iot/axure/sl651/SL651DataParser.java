/*
package com.ruoyi.iot.axure.sl651;

import com.alibaba.fastjson.JSON;
import com.shuize.dmp.common.util.DataFrameUtil;
import com.shuize.dmp.common.util.DateUtil;
import com.shuize.dmp.monitordata.entity.*;
import com.shuize.dmp.monitordata.netty.server.template.*;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.text.ParseException;
import java.util.*;

import static com.shuize.dmp.monitordata.netty.server.template.SL651Constant.CONTROL_CHAR_SOH;
import static com.shuize.dmp.monitordata.netty.server.template.SL651Constant.CONTROL_CHAR_START_SYN;
import static com.shuize.dmp.monitordata.netty.server.template.SL651IdentifierEnum.*;

*/
/**
 * @ClassName SL651DataParser
 * @Description SL651数据解析
 * Date 2023/1/10 16:56
 * Version 1.0
 **//*

@Slf4j
@Component
public class SL651DataParser<T> implements DataParser {

    */
/**
     * 完整报文最小长度 = 链路维持报长度:25
     *//*

    private final static int SL651_MIN_LENGTH = 25;

    private static SL651DataParser sl651DataParser;

    @PostConstruct
    void init() {
        sl651DataParser = this;
    }

    @Autowired
    private SL651Template template;

    */
/**
     * SL651协议包完整性校验 CRC计算结果与报文携带CRC校验码比对
     *
     * @param dataFrame
     * @return
     *//*

    @Override
    public boolean check(DataFrame dataFrame) {
        //获取校验码前的所有数字
        String substring = dataFrame.valueToHex().substring(0, dataFrame.valueToHex().length() - 4);
        //将字符串转化为16进制字节数组
        byte[] hexBytes = DataFrameUtil.hexStringToByteArray(substring);
        //crc校验
        String computeCrc = DataFrameUtil.getCRC(hexBytes);
        //原先crc
        String crcCode = dataFrame.getDataField().getCrcCode().valueToHex();
        return computeCrc.equalsIgnoreCase(crcCode);
    }

    */
/**
     * 解码报文 并转成DataFrame对象输出
     *
     * @param in
     * @param out
     * @return
     *//*

    @Override
    public boolean decode(ByteBuf in, List<Object> out) {
        log.info("in:{}, {}", in.readableBytes(), ByteBufUtil.hexDump(in, in.readerIndex(), in.readableBytes()).toUpperCase());
        if (in.readableBytes() < SL651_MIN_LENGTH) {
            log.error("Msg len:{} less than SL651_MIN_LENGTH:{}.", in.readableBytes(), SL651_MIN_LENGTH);
            return false;
        } else {
            try {
                while (in.readableBytes() >= SL651_MIN_LENGTH) {
                    DataFrame dataFrame = decodeSL651(in);
                    if (dataFrame != null) {
                        out.add(dataFrame);
                        log.info("after out unread:{}", in.readableBytes());
                    } else {
                        log.info("Incomplete pkg msg.");
                        return false;
                    }
                }
            } catch (Exception e) {
                log.error("decode failed:{}", in.readableBytes());
                in.clear();
                return false;
            }
        }
        return true;
    }


    */
/**
     * 报文解析 将dataFrame对象并转换成dataMap key为元素标识符，value为对应值
     *
     * @param dataFrame
     * @return
     *//*

    @Override
    public Map<String, Object> parse(DataFrame dataFrame) {
        List<DataUnit> header = dataFrame.getHeader();
        //测站编码
        String stCode = header.get(2).valueToHex();
        //功能码
        String funCode = header.get(4).valueToHex().toUpperCase();
        //报文起始符
        String startChar = header.get(6).valueToHex();
        //分包发送报文报总数pkgTotal及序列号pkgSerial
        int pkgUnitVal = header.get(7).valueToInt();
        int pkgTotal = pkgUnitVal > 0 ? pkgUnitVal >> 12 : 0;
        Integer pkgSerial = pkgUnitVal > 0 ? pkgUnitVal & 4095 : 0;
        //报文结束符
        String endChar = dataFrame.getDataField().getDataTail().valueToHex();
        //数据正文
        String dataContent = dataFrame.getDataField().getDataParamsMap().get("data").get(0).valueToHex();
        log.debug("dataContent:{}", dataContent);
        StringBuilder sb = new StringBuilder(dataContent);
        //获取2字节流水号 分包数据为同一组 每组流水号相同
        int serialNum = Integer.parseInt(fetchDataContent(sb, 2), 16);
        //获取6字节发报日期
        String reportDateStr = fetchDataContent(sb, 6);
        Map<String, Object> dataMap = null;
        switch (SL651FunCode.valueOf("F_" + funCode)) {
            case F_30:
                String identifier = dataContent.substring(0, 4);
                String stCodeHex = dataContent.substring(4, 9);
                dataContent = dataContent.substring(9);
                parseTestMessage(sb);
                break;
            case F_31:
                try {
                    dataMap = parseREPORT31(sb);
                } catch (ParseException e) {
                    log.error("F_31 parse date err, {}", e.getMessage(), e);
                } catch (ClassNotFoundException e) {
                    log.error("F_31 parse value err, {}", e.getMessage(), e);
                }
                break;
            case F_32:
                try {
                    dataMap = parseREPORT32(sb);
                } catch (ParseException e) {
                    log.error("F_32 parse date err, {}", e.getMessage(), e);
                } catch (ClassNotFoundException e) {
                    log.error("F_32 parse value err, {}", e.getMessage(), e);
                }
                break;
            case F_33:
                try {
                    dataMap = parseREPORT33(sb);
                } catch (ParseException e) {
                    log.error("F_33 parse date err, {}", e.getMessage(), e);
                } catch (ClassNotFoundException e) {
                    log.error("F_33 parse value err, {}", e.getMessage(), e);
                }
                break;
            case F_34:
                try {
                    dataMap = parseREPORT34(sb);
                } catch (ParseException e) {
                    log.error("F_34 parse date err, {}", e.getMessage(), e);
                } catch (ClassNotFoundException e) {
                    log.error("F_34 parse value err, {}", e.getMessage(), e);
                }
                break;
            case F_35:
                //人工置数报
                try {
                    dataMap = parseREPORT35(sb);
                } catch (ParseException e) {
                    log.error("F_35 parse date err, {}", e.getMessage(), e);
                } catch (ClassNotFoundException e) {
                    log.error("F_35 parse value err, {}", e.getMessage(), e);
                }
                break;
            case F_36:
                dataMap = parseImageMessage(sb);
                if (!CollectionUtils.isEmpty(dataMap)) {
                    dataMap.put(I_E1.getCode(), serialNum);
                    dataMap.put(I_E2.getCode(), pkgTotal);
                    dataMap.put(I_E3.getCode(), pkgSerial);
                }
                break;
            case F_2F:
            default:
                log.info("funCode:{}, stCode:{}", funCode, stCode);
                dataMap = null;
        }
        return dataMap;
    }

    */
/**
     * 获取测站编码
     *
     * @param stCode
     * @return
     *//*

    @Override
    public String getStCode(String stCode) {
        return stCode.startsWith("00") ? stCode : stCode.substring(0, 6) + String.format("%06d", Integer.parseInt(stCode.substring(6), 16));
    }

    public List<Object> dataMapSolver(Map<String, Object> dataMap) {
        List<Object> result = new ArrayList();
        try {
            switch (SL651StTypeEnum.valueOf((String) dataMap.get("I_" + I_E0.getCode()))) {
                //降水
                case S_50:
                    result = s50Solver(dataMap);
                    break;
                //河道水情
                case S_48:
                    result = s48Solver(dataMap);
                    break;
                //水库水情
                case S_4B:
                    result = s4BSolver(dataMap);
                    break;
                //闸坝水情
                case S_5A:
                    result = s5ASolver(dataMap);
                    break;
                //墒情
                case S_4D:
                    result = s4DSolver(dataMap);
                    break;
                //地下水
                case S_47:
                    result = s47Solver(dataMap);
                    break;
                //取水口
                case S_49:
                    //排水口
                case S_4F:
                    //泵站水情
                case S_44:
                    //潮汐
                case S_54:
                    //水质
                case S_51:
                default:
                    log.debug("dataMap: {}", JSON.toJSONString(dataMap));
            }
        } catch (ParseException | ClassNotFoundException e) {
            log.error("dataMap check failed: {}", dataMap);
        }
        return result;
    }

    */
/**
     * 河道站数据解析 测站类型：48
     *
     * @param dataMap
     * @return
     * @throws ParseException
     * @throws ClassNotFoundException
     *//*

    private List<Object> s48Solver(Map<String, Object> dataMap) throws ParseException, ClassNotFoundException {
        List<Object> result = null;
        if (!dataMap.isEmpty()) {
            result = new ArrayList<>();
            List<PrecipitationEntity> pptns = pptnData(dataMap);
            if (!CollectionUtils.isEmpty(pptns)) {
                result.add(pptns);
            }
            EvaporationEntity evaporation = evaporationData(dataMap);
            if (evaporation != null) {
                result.add(evaporation);
            }
            WindSpeedAndDirectionEntity wind = windData(dataMap);
            if (wind != null) {
                result.add(wind);
            }
            List<ResHydroInfoEntity> resHydros = reservoirRegimeData(dataMap);
            if (!CollectionUtils.isEmpty(resHydros)) {
                result.add(resHydros);
            }
            EcologicalFlowEntity ecologicalFlow = ecologicalFlowData(dataMap);
            if (ecologicalFlow != null) {
                result.add(ecologicalFlow);
            }
            List<Object> qList = waterQualityData(dataMap);
            if (!CollectionUtils.isEmpty(qList)) {
                result.add(qList);
            }
            WaterAndAirTempEntity waterAndAirTemp = waterAndAirTempData(dataMap);
            if (waterAndAirTemp != null) {
                result.add(waterAndAirTemp);
            }
        }
        return result;
    }

    */
/**
     * 水库（湖泊）数据解析 测站类型：4B
     *
     * @param dataMap
     * @return
     * @throws ParseException
     * @throws ClassNotFoundException
     *//*

    private List<Object> s4BSolver(Map<String, Object> dataMap) throws ParseException, ClassNotFoundException {
        List<Object> result = null;
        if (!dataMap.isEmpty()) {
            result = new ArrayList<>();
            List<PrecipitationEntity> pptns = pptnData(dataMap);
            if (!CollectionUtils.isEmpty(pptns)) {
                result.add(pptns);
            }
            EvaporationEntity evaporation = evaporationData(dataMap);
            if (evaporation != null) {
                result.add(evaporation);
            }
            WindSpeedAndDirectionEntity wind = windData(dataMap);
            if (wind != null) {
                result.add(wind);
            }
            List<ResHydroInfoEntity> resHydros = reservoirRegimeData(dataMap);
            if (!CollectionUtils.isEmpty(resHydros)) {
                result.add(resHydros);
            }
            List<Object> qlist = waterQualityData(dataMap);
            if (!CollectionUtils.isEmpty(qlist)) {
                result.add(qlist);
            }
            WaterAndAirTempEntity waterAndAirTemp = waterAndAirTempData(dataMap);
            if (waterAndAirTemp != null) {
                result.add(waterAndAirTemp);
            }
        }
        return result;
    }

    */
/**
     * 闸坝数据解析 测站类型：5A
     *
     * @param dataMap
     * @return
     * @throws ParseException
     * @throws ClassNotFoundException
     *//*

    public List<Object> s5ASolver(Map<String, Object> dataMap) throws ParseException, ClassNotFoundException {
        List<Object> result = null;
        if (!dataMap.isEmpty()) {
            result = new ArrayList<>();
            List<PrecipitationEntity> pptns = pptnData(dataMap);
            if (!CollectionUtils.isEmpty(pptns)) {
                result.add(pptns);
            }
            EvaporationEntity evaporation = evaporationData(dataMap);
            if (evaporation != null) {
                result.add(evaporation);
            }
            WindSpeedAndDirectionEntity wind = windData(dataMap);
            if (wind != null) {
                result.add(wind);
            }
            GateOpeningEntity gateOpening = gateAndDamRegimeData(dataMap);
            if (gateOpening != null) {
                result.add(gateOpening);
            }
            List<Object> waterQs = waterQualityData(dataMap);
            if (!CollectionUtils.isEmpty(waterQs)) {
                result.add(waterQs);
            }
            WaterAndAirTempEntity waterAndAirTemp = waterAndAirTempData(dataMap);
            if (waterAndAirTemp != null) {
                result.add(waterAndAirTemp);
            }
        }
        return result;
    }

    */
/**
     * 降水站数据解析 测站类型：50
     *
     * @param dataMap
     * @return
     * @throws ParseException
     * @throws ClassNotFoundException
     *//*

    private List<Object> s50Solver(Map<String, Object> dataMap) throws ParseException, ClassNotFoundException {
        List<Object> result = null;
        if (!dataMap.isEmpty()) {
            result = new ArrayList<>();
            List<PrecipitationEntity> pptns = pptnData(dataMap);
            if (!CollectionUtils.isEmpty(pptns)) {
                result.add(pptns);
            }
            EvaporationEntity evaporation = evaporationData(dataMap);
            if (evaporation != null) {
                result.add(evaporation);
            }
            WindSpeedAndDirectionEntity wind = windData(dataMap);
            if (wind != null) {
                result.add(wind);
            }
            WaterAndAirTempEntity waterAndAirTemp = waterAndAirTempData(dataMap);
            if (waterAndAirTemp != null) {
                result.add(waterAndAirTemp);
            }
        }
        return result;
    }

    */
/**
     * 墒情站数据解析 测站类型：4D
     *
     * @param dataMap
     * @return
     * @throws ParseException
     * @throws ClassNotFoundException
     *//*

    public List<Object> s4DSolver(Map<String, Object> dataMap) throws ParseException, ClassNotFoundException {
        List<Object> result = null;
        if (!dataMap.isEmpty()) {
            List<PrecipitationEntity> pptns = pptnData(dataMap);
            if (!CollectionUtils.isEmpty(pptns)) {
                result.add(pptns);
            }
            EvaporationEntity evaporation = evaporationData(dataMap);
            if (evaporation != null) {
                result.add(evaporation);
            }
            SoilMoistureEntity soilMoisture = soilMoistureData(dataMap);
            if (soilMoisture != null) {
                result.add(soilMoisture);
            }
        }
        return result;
    }

    public List<Object> s47Solver(Map<String, Object> dataMap) throws ParseException, ClassNotFoundException {
        List<Object> result = null;
        if (!dataMap.isEmpty()) {
            result = new ArrayList<>();
            GroundWaterEntity groundWater = groundWaterData(dataMap);
            if (groundWater != null) {
                result.add(groundWater);
            }
            WaterAndAirTempEntity waterAndAirTemp = waterAndAirTempData(dataMap);
            if (waterAndAirTemp != null) {
                result.add(waterAndAirTemp);
            }
        }
        return result;
    }

    private DataFrame decodeSL651(ByteBuf buf) throws Exception {
        //获取报文帧起始符
        String soh = ByteBufUtil.hexDump(buf, buf.readerIndex(), 2);
        DataFrame dataFrame;
        if (CONTROL_CHAR_SOH.equalsIgnoreCase(soh)) {
            //复制报文数据帧对象模板
            dataFrame = SerializationUtils.clone(sl651DataParser.template.getDataFrame());
            //获取正文起始符
            String startStr = ByteBufUtil.hexDump(buf, 13, 1);
            int headerLen = 7;
            if (CONTROL_CHAR_START_SYN.equalsIgnoreCase(startStr)) {
                headerLen++;
            }
            //报文头解码 填充header各属性value
            List<DataUnit> header = dataFrame.getHeader();
            DataUnit pkgUnit = header.get(7);
            for (int i = 0; i < headerLen; i++) {
                if (i == 7) {
                    pkgUnit.setLength(3);
                }
                fill(header.get(i), buf);
            }
            // 获取数据域正文长度
            int len = Integer.parseInt(header.get(5).valueToHex(), 16);
            // 获取剩余未读数据长度
            int unreadLen = buf.readableBytes();
            //判断剩余可读字节数是否小于正文长度
            if (unreadLen < len) {
                buf.resetReaderIndex();
                dataFrame = null;
            } else {
                //获取数据域对象，并填充值
                DataField field = dataFrame.getDataField();
                //获取正文对象
                List<DataUnit> data = field.getDataParamsMap().get("data");
                //正文填充值
                data.get(0).setLength(len - pkgUnit.getLength());
                fill(data.get(0), buf);
                //填充数据域帧尾、crc校验码
                fill(field.getDataTail(), buf);
                fill(field.getCrcCode(), buf);
            }
        } else {
            throw new Exception("Illegal msg header:" + soh);
        }
        return dataFrame;
    }

    private void fill(DataUnit unit, ByteBuf buf) {
        int length = unit.getLength();
        if (length > 0) {
            byte[] bytes = new byte[length];
            buf.readBytes(bytes);
            unit.setValue(bytes);
        }
    }

    */
/**
     * 均匀时段水文信息报
     *//*

    public static Map<String, Object> parseREPORT31(StringBuilder sb) throws ParseException, ClassNotFoundException {
        Map<String, Object> dataMap = new HashMap<String, Object>();
        while (sb.length() > 0) {
            SL651Identifer identifer = getIdentifier(sb);
            if (I_F1.getCode().equals(identifer.getCode()) || I_F0.getCode().equals(identifer.getCode())) {
                identifer.setLength(5);
                identifer.setAccuracy(0);
                String dataHex = fetchDataContent(sb, identifer.getLength());
                if (I_F1.getCode().equals(identifer.getCode())) {
                    dataMap.put("I_" + identifer.getCode(), dataHex);
                    String stType = fetchDataContent(sb, 1);
                    dataMap.put("I_" + I_E0.getCode(), "S_" + stType);
                } else {
                    Date value = DataDecodeUtil.value(dataHex, SL651IdentifierEnum.valueOf("I_" + identifer.getCode()));
                    dataMap.put("I_" + identifer.getCode() + "1", value);
                    SL651Identifer identifer2 = getIdentifier(sb);
                    String steptime = fetchDataContent(sb, identifer2.getLength());
                    dataMap.put("I_" + identifer2.getCode(), steptime);
                }
                continue;
            }
            int i = sb.length() / (identifer.getLength() * 2);
            //当前时间
            Date date = (Date) dataMap.get("I_F01");
            //时间步长码
            Integer steptime = Integer.parseInt((String) dataMap.get("I_04"), 16);
            for (int j = 1; j <= i; j++) {
                String dataHex = fetchDataContent(sb, identifer.getLength());
                if (j != 1) {
                    dataMap.put("I_F0" + j, date);
                }
                dataMap.put("I_" + identifer.getCode() + j, dataHex);
                date = DateUtil.getAfterSecondDate(date, steptime * 60);
            }
        }
        return dataMap;
    }

    */
/**
     * 定时报
     *//*

    public static Map<String, Object> parseREPORT32(StringBuilder sb) throws ParseException, ClassNotFoundException {
        Map<String, Object> dataMap = new HashMap<String, Object>();
        while (sb.length() > 0) {
            SL651Identifer identifer = getIdentifier(sb);
            if (I_F1.getCode().equals(identifer.getCode()) || I_F0.getCode().equals(identifer.getCode())) {
                identifer.setLength(5);
                identifer.setAccuracy(0);
                String dataHex = fetchDataContent(sb, identifer.getLength());
                if (I_F1.getCode().equals(identifer.getCode())) {
                    dataMap.put("I_" + identifer.getCode(), dataHex);
                    String stType = fetchDataContent(sb, 1);
                    dataMap.put("I_" + I_E0.getCode(), "S_" + stType);
                } else {
                    Date value = DataDecodeUtil.value(dataHex, SL651IdentifierEnum.valueOf("I_" + identifer.getCode()));
                    dataMap.put("I_" + identifer.getCode(), value);
                }
                continue;
            }
            String dataHex = fetchDataContent(sb, identifer.getLength());
            dataMap.put("I_" + identifer.getCode(), dataHex);
        }
        return dataMap;
    }

    */
/**
     * 加报报
     *//*

    public static Map<String, Object> parseREPORT33(StringBuilder sb) throws ParseException, ClassNotFoundException {
        Map<String, Object> dataMap = new HashMap<String, Object>();
        while (sb.length() > 0) {
            SL651Identifer identifer = getIdentifier(sb);
            if (I_F1.getCode().equals(identifer.getCode()) || I_F0.getCode().equals(identifer.getCode())) {
                identifer.setLength(5);
                identifer.setAccuracy(0);
                String dataHex = fetchDataContent(sb, identifer.getLength());
                if (I_F1.getCode().equals(identifer.getCode())) {
                    dataMap.put("I_" + identifer.getCode(), dataHex);
                    String stType = fetchDataContent(sb, 1);
                    dataMap.put("I_" + I_E0.getCode(), "S_" + stType);
                } else {
                    Date value = DataDecodeUtil.value(dataHex, SL651IdentifierEnum.valueOf("I_" + identifer.getCode()));
                    dataMap.put("I_" + identifer.getCode(), value);
                }
                continue;
            }
            String dataHex = fetchDataContent(sb, identifer.getLength());
            dataMap.put("I_" + identifer.getCode(), dataHex);

        }
        return dataMap;
    }

    */
/**
     * 小时报
     *//*

    public static Map<String, Object> parseREPORT34(StringBuilder sb) throws ParseException, ClassNotFoundException {
        Map<String, Object> dataMap = new HashMap<String, Object>();
        int sum = 1;
        while (sb.length() > 0) {
            SL651Identifer identifer = getIdentifier(sb);
            if (I_F1.getCode().equals(identifer.getCode()) || I_F0.getCode().equals(identifer.getCode())) {
                identifer.setLength(5);
                identifer.setAccuracy(0);
                String dataHex = fetchDataContent(sb, identifer.getLength());
                if (I_F1.getCode().equals(identifer.getCode())) {
                    dataMap.put("I_" + identifer.getCode(), dataHex);
                    String stType = fetchDataContent(sb, 1);
                    dataMap.put("I_" + I_E0.getCode(), "S_" + stType);
                    continue;
                } else {
                    Date value = DataDecodeUtil.value(dataHex, SL651IdentifierEnum.valueOf("I_" + identifer.getCode()));
                    dataMap.put("I_" + identifer.getCode() + sum, value);
                    SL651Identifer identifer1 = getIdentifier(sb);
                    String dataHex1 = fetchDataContent(sb, identifer1.getLength());
                    dataMap.put("I_" + identifer1.getCode() + sum, dataHex1);
                }
            } else {
                Date date = (Date) dataMap.get("I_F0" + (sum - 1));
                dataMap.put("I_F0" + sum, date);
                String dataHex = fetchDataContent(sb, identifer.getLength());
                dataMap.put("I_" + identifer.getCode() + sum, dataHex);
            }
            sum++;
        }
        return dataMap;
    }

    */
/**
     * 人工置数报
     *//*

    public static Map<String, Object> parseREPORT35(StringBuilder sb) throws ParseException, ClassNotFoundException {
        Map<String, Object> dataMap = new HashMap<String, Object>();
        SL651Identifer identifer = getIdentifier(sb);
        String dataHex = fetchDataContent(sb, identifer.getLength());
        dataMap.put("I_" + identifer.getCode(), dataHex);
        //数据 HEX转换为字节数据 再转换为字符串
        return dataMap;
    }


    */
/**
     * 根据数据长度截取数据Hex字符串
     *
     * @param sb
     * @param len
     * @return
     *//*

    public static String fetchDataContent(StringBuilder sb, int len) {
        String data = null;
        //1字节对应2位hex
        len *= 2;
        if (len > 0 && sb != null && sb.length() >= len) {
            data = sb.substring(0, len);
            sb.delete(0, len);
        }
        return data.toUpperCase();
    }

    */
/**
     * 解析报文正文 获取要素标识对象
     *
     * @param sb
     * @return
     *//*

    public static SL651Identifer getIdentifier(StringBuilder sb) {
        SL651Identifer identifer = null;
        if (sb != null && sb.length() > 0) {
            identifer = new SL651Identifer();
            //解析标识符
            String code = fetchDataContent(sb, 1).toUpperCase();
            if ("FF".equals(code)) {
                code += fetchDataContent(sb, 1).toUpperCase();
            }
            identifer.setCode(code);
            //解析数据定义
            int dataDef = Integer.parseInt(fetchDataContent(sb, 1), 16);
            //数据长度
            if (code.startsWith("FF")) {
//                dataDef = Integer.parseInt(SL651IdentifierEnum.valueOf("I_" + code).getUnit());
                identifer.setLength(28);
            } else {
                identifer.setLength(dataDef >> 3);
            }
            //数据精度
            identifer.setAccuracy(dataDef & (0xFF >> 5));
            log.debug("identifer:{}", JSON.toJSONString(identifer));
        }
        return identifer;
    }

    */
/**
     * SL651测试报解析
     *
     * @param sb
     * @return 返回HashMap key:元素标识符 value:对应元素值的字符串
     *//*

    public static Map<String, Object> parseTestMessage(StringBuilder sb) {
        Map<String, Object> dataMap = new HashMap<String, Object>();
        boolean nextType = false;
        while (sb.length() > 0) {
            SL651Identifer identifer = getIdentifier(sb);
            if (I_F1.getCode().equals(identifer.getCode()) || I_F0.getCode().equals(identifer.getCode())) {
                nextType = !nextType;
                identifer.setLength(5);
                identifer.setAccuracy(0);
            }
            String dataHex = fetchDataContent(sb, identifer.getLength());
            dataMap.put("I_" + identifer.getCode(), dataHex);
            if (nextType) {
                dataMap.put("I_" + I_E0.getCode(), "S_" + fetchDataContent(sb, 1));
            }
        }
        log.info("test msg map: {}", JSON.toJSONString(dataMap));
        return dataMap;
    }

    */
/**
     * SL651图片报解析
     *
     * @param sb
     * @return 返回HashMap key:元素标识符 value:对应元素值的字符串
     *//*

    public static Map<String, Object> parseImageMessage(StringBuilder sb) {
        Map<String, Object> dataMap = new HashMap<>();
        boolean nextType = false;
        while (sb.length() > 0) {
            SL651Identifer identifer = getIdentifier(sb);
            if (I_F1.getCode().equals(identifer.getCode()) || I_F0.getCode().equals(identifer.getCode())) {
                nextType = !nextType;
                identifer.setLength(5);
                identifer.setAccuracy(0);
            }
            if (I_F3.getCode().equals(identifer.getCode())) {
                identifer.setLength(sb.length() / 2);
                identifer.setAccuracy(0);
            }
            String dataHex = fetchDataContent(sb, identifer.getLength());
            dataMap.put(identifer.getCode(), dataHex);
            if (nextType) {
                dataMap.put("I_" + I_E0.getCode(), fetchDataContent(sb, 1));
            }
        }
        return dataMap;
    }

    */
/**
     * @param dataMap 解析报文正文 获取降水信息对象
     * @return List<PrecipitationEntity> 返回雨量对象列表
     * @throws ParseException
     * @throws ClassNotFoundException
     *//*

    public List<PrecipitationEntity> pptnData(Map<String, Object> dataMap) throws ParseException, ClassNotFoundException {
        List<PrecipitationEntity> pptnList = null;
        if (!dataMap.isEmpty()) {
            pptnList = new ArrayList<PrecipitationEntity>();
            Double dyp = null;
            Double ap = null;
            Date dates = null;
            for (String key : dataMap.keySet()) {
                switch (SL651IdentifierEnum.valueOf(key.length() % 2 == 0 ? key : key.substring(0, key.length() - 1))) {
                    case I_20:
                    case I_1F:
                        dyp = DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4)));
                        dates = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_21:
                    case I_22:
                    case I_23:
                    case I_24:
                    case I_1A:
                    case I_1B:
                    case I_1C:
                    case I_1D:
                    case I_1E:
                        PrecipitationEntity pptn = new PrecipitationEntity();
                        pptn.setIntv(Integer.parseInt(SL651IdentifierEnum.valueOf(key.substring(0, 4)).getDescription()));
                        pptn.setDrp(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        Date date = (Date) dataMap.get("I_F0" + key.substring(4));
                        pptn.setRecordDate(date);
                        pptnList.add(pptn);
                        break;
                    case I_F4:
                        List<Double> data = DataDecodeUtil.valueGroup((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4)));
//                        Date dt = (Date) dataMap.get("I_F0" + key.substring(4));
                        Date dt = DateUtil.getLastHourDate((Date) dataMap.get("I_F0" + key.substring(4)), 1);
                        for (Double b : data) {
                            PrecipitationEntity p = new PrecipitationEntity();
                            p.setDrp(b);
                            p.setIntv(Integer.parseInt(SL651IdentifierEnum.valueOf(key.substring(0, 4)).getDescription()));
                            p.setRecordDate(dt);
                            pptnList.add(p);
                            dt = DateUtil.getAfterSecondDate(dt, 300);
                        }
                        break;
                    case I_26:
                        ap = (DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        dates = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_25:
                        StormEntity stormEntity = new StormEntity();
                        stormEntity.setStrmdr(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        stormEntity.setRecordDate((Date) dataMap.get("I_F0" + key.substring(4)));
                        stormEntity.setStCode(getStCode((String) dataMap.get("I_F1")));
                        log.info("StormEntity:{}", stormEntity.toString());
                    default:
                }
            }
            if (pptnList.size() > 0) {
                for (PrecipitationEntity ppt : pptnList) {
                    if (dyp != null) {
                        ppt.setDyp(dyp);
                    }
                    if (ap != null) {
                        ppt.setAp(ap);
                    }
                    ppt.setStCode(getStCode((String) dataMap.get("I_F1")));
                    log.info("pptn:{}", ppt.toString());
                }
            } else {
                if (dyp != null || ap != null) {
                    PrecipitationEntity pptn = new PrecipitationEntity();
                    pptn.setAp(ap);
                    pptn.setDyp(dyp);
                    pptn.setRecordDate(dates);
                    pptn.setStCode(getStCode((String) dataMap.get("I_F1")));
                    pptn.setIntv(0);
                    log.info("precipitationEntity:{}", pptn.toString());
                    pptnList.add(pptn);
                }
            }
        }
        return pptnList;
    }

    */
/**
     * 解析报文正文 获取蒸发信息对象
     *
     * @param dataMap
     * @return
     * @throws ParseException
     * @throws ClassNotFoundException
     *//*

    public EvaporationEntity evaporationData(Map<String, Object> dataMap) throws ParseException, ClassNotFoundException {
        EvaporationEntity evaporation = null;
        if (!dataMap.isEmpty()) {
            Date date = null;
            Double dailyEva = null;
            for (String key : dataMap.keySet()) {
                switch (SL651IdentifierEnum.valueOf(key.length() % 2 == 0 ? key : key.substring(0, key.length() - 1))) {
                    case I_06:
                    case I_07:
                        dailyEva = (DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    default:
                }
            }
            if (dailyEva != null) {
                evaporation.setStCode(getStCode((String) dataMap.get("I_F1")));
                evaporation.setDailyEvaporation(dailyEva);
                evaporation.setRecordDate(date);
                log.info("evaporationEntity:{}", JSON.toJSONString(evaporation));
            }
        }
        return evaporation;
    }

    */
/**
     * 解析报文正文 获取气象信息对象
     *
     * @param dataMap
     * @return 返回风速风向对象
     * @throws ParseException
     * @throws ClassNotFoundException
     *//*

    public WindSpeedAndDirectionEntity windData(Map<String, Object> dataMap) throws ParseException, ClassNotFoundException {
        WindSpeedAndDirectionEntity wind = null;
        if (!dataMap.isEmpty()) {
            Integer windDirection = null;
            Double WindSpeed = null;
            Date date = null;
            for (String key : dataMap.keySet()) {
                switch (SL651IdentifierEnum.valueOf(key.length() % 2 == 0 ? key : key.substring(0, key.length() - 1))) {
                    case I_33:
                        windDirection = DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4)));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_35:
                        WindSpeed = (DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    default:
                }
            }
            if (windDirection != null || WindSpeed != null) {
                wind = new WindSpeedAndDirectionEntity();
                wind.setStCode(getStCode((String) dataMap.get("I_F1")));
                wind.setWindDirection(windDirection);
                wind.setWindSpeed(WindSpeed);
                wind.setWindPower(WindSpeed != null ? getWindPower(WindSpeed) : null);
                wind.setRecordDate(date);
                log.info("windSpeedAndDirectionEntity:{}", JSON.toJSONString(wind));
            }
        }
        return wind;
    }

    */
/**
     * 解析报文正文 获取河道水情信息对象
     *//*

    public EcologicalFlowEntity ecologicalFlowData(Map<String, Object> dataMap) throws ParseException, ClassNotFoundException {
        EcologicalFlowEntity ecologicalFlow = null;
        if (!dataMap.isEmpty()) {
            Date date = null;
            for (String key : dataMap.keySet()) {
                switch (SL651IdentifierEnum.valueOf(key.length() % 2 == 0 ? key : key.substring(0, key.length() - 1))) {
                    case I_27:
                        ecologicalFlow.setInstantFlow(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_37:
                        ecologicalFlow.setFlowRate(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    default:
                }
            }
            if (date != null) {
                ecologicalFlow = new EcologicalFlowEntity();
                ecologicalFlow.setRecordDate(date);
                ecologicalFlow.setStCode(getStCode((String) dataMap.get("I_F1")));
                log.info("EcologicalFlowEntity:{}", JSON.toJSONString(ecologicalFlow));
            }
        }
        return ecologicalFlow;
    }

    */
/**
     * 解析报文正文 获取水质信息对象
     *//*

    public List<Object> waterQualityData(Map<String, Object> dataMap) throws ParseException, ClassNotFoundException {
        List<Object> waterQualityList = new ArrayList<>();
        WaterQualityCommonEntity waterQualityC = null;
        WaterQualityRoutineEntity waterQualityR = null;
        if (!dataMap.isEmpty()) {
            Date date = null;
            for (String key : dataMap.keySet()) {
                switch (SL651IdentifierEnum.valueOf(key.length() % 2 == 0 ? key : key.substring(0, key.length() - 1))) {
                    case I_4D:
                        waterQualityC.setTotalPhosphorus(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_4E:
                        waterQualityC.setTotalNitrogen(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_4C:
                        waterQualityC.setNh3N(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_4A:
                        waterQualityC.setKMno(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_03:
                        waterQualityR.setWaterTemp(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_46:
                        waterQualityR.setPh(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_47:
                        waterQualityR.setDissolvedOxygen(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_48:
                        waterQualityR.setConductivity(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_49:
                        waterQualityR.setTurbidity(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    default:
                }
            }
            if (date != null) {
                waterQualityC.setRecordDate(date);
                waterQualityR.setRecordDate(date);
                waterQualityC.setStCode(getStCode((String) dataMap.get("I_F1")));
                waterQualityR.setStCode(getStCode((String) dataMap.get("I_F1")));
                log.info("waterQualityC:{}", waterQualityC.toString());
                log.info("waterQualityR:{}", waterQualityR.toString());
                waterQualityList.add(waterQualityC);
                waterQualityList.add(waterQualityR);
            }
        }
        return waterQualityList;
    }

    */
/**
     * 解析报文正文 获取水库水情信息对象
     *
     * @return
     *//*

    public List<ResHydroInfoEntity> reservoirRegimeData(Map<String, Object> dataMap) throws ParseException, ClassNotFoundException {
        List<ResHydroInfoEntity> resHydroList = null;
        if (!dataMap.isEmpty()) {
            resHydroList = new ArrayList<>();
            Double belowres = null;
            Double res = null;
            Double outflow = null;
            Double inflow = null;
            Date date = null;
            for (String key : dataMap.keySet()) {
                switch (SL651IdentifierEnum.valueOf(key.length() % 2 == 0 ? key : key.substring(0, key.length() - 1))) {
                    case I_3A:
                        belowres = (DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_3B:
                        res = (DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_3C:
                        res = (DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_30:
                        outflow = (DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_31:
                        inflow = (DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_F5:
                    case I_F6:
                    case I_F7:
                    case I_F8:
                    case I_F9:
                    case I_FA:
                    case I_FB:
                    case I_FC:
                        List<Double> data = DataDecodeUtil.valueGroup((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4)));
                        Date dates = (Date) dataMap.get("I_F0" + key.substring(4));
                        for (Double b : data) {
                            ResHydroInfoEntity resHydro = new ResHydroInfoEntity();
                            resHydro.setResWaterLevel(b);
                            resHydro.setRecordDate(dates);
                            resHydroList.add(resHydro);
                            dates = DateUtil.getAfterSecondDate(dates, 300);
                        }
                        break;
                    case I_FF07:
                        Double wes = DataDecodeUtil.value(((String) dataMap.get(key)).substring(0, 8), valueOf(key.substring(0, 6)));
                        List<Double> datas = DataDecodeUtil.valueGroup(((String) dataMap.get(key)).substring(8), SL651IdentifierEnum.valueOf(key.substring(0, 6)));
                        Date date2 = (Date) dataMap.get("I_F0" + key.substring(6));
                        for (Double b : datas) {
                            ResHydroInfoEntity resHydro = new ResHydroInfoEntity();
                            resHydro.setResWaterLevel(b + wes);
                            resHydro.setRecordDate(date2);
                            resHydroList.add(resHydro);
                            date2 = DateUtil.getAfterMinDate(date2, 5);
                        }
                        break;
                    default:
                }
            }
            if (resHydroList.size() > 0) {
                for (ResHydroInfoEntity resEntity : resHydroList) {
                    resEntity.setStCode(getStCode((String) dataMap.get("I_F1")));
                }
            } else {
                if (belowres != null || res != null || outflow != null || inflow != null) {
                    ResHydroInfoEntity resHydro = new ResHydroInfoEntity();
                    resHydro.setBelowResWaterLevel(belowres);
                    resHydro.setResWaterLevel(res);
                    resHydro.setOutFlow(outflow);
                    resHydro.setInFLow(inflow);
                    resHydro.setStCode(getStCode((String) dataMap.get("I_F1")));
                    resHydro.setRecordDate(date);
                    resHydroList.add(resHydro);
                }
            }
            log.info("resHydro:{}", JSON.toJSONString(resHydroList));
        }
        return resHydroList;
    }

    */
/**
     * 解析报文正文 获取闸坝水情信息对象
     *//*

    public GateOpeningEntity gateAndDamRegimeData(Map<String, Object> dataMap) throws ParseException, ClassNotFoundException {
        GateOpeningEntity gateOpening = null;
        if (!dataMap.isEmpty()) {
            gateOpening = new GateOpeningEntity();
            Date date = null;
            for (String key : dataMap.keySet()) {
                switch (SL651IdentifierEnum.valueOf(key.length() % 2 == 0 ? key : key.substring(0, key.length() - 1))) {
                    case I_0A:
                        gateOpening.setWagaNumber(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_09:
                        gateOpening.setHeight(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        break;
                    default:
                }
            }
            if (date != null) {
                gateOpening.setRecordDate(date);
                gateOpening.setStCode(getStCode((String) dataMap.get("I_F1")));
                log.info("GateOpeningEntity:{}", gateOpening.toString());
            }
        }
        return gateOpening;
    }

    */
/**
     * 解析报文正文 获取地下水水情信息对象
     *
     * @return
     *//*

    public GroundWaterEntity groundWaterData(Map<String, Object> dataMap) throws ParseException, ClassNotFoundException {
        GroundWaterEntity groundWater = null;
        if (!dataMap.isEmpty()) {
            groundWater = new GroundWaterEntity();
            Date date = null;
            for (String key : dataMap.keySet()) {
                switch (SL651IdentifierEnum.valueOf(key.length() % 2 == 0 ? key : key.substring(0, key.length() - 1))) {
                    case I_0E:
                        groundWater.setDepth(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_0D:
                        groundWater.setGroundWaterTemp(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        break;
                    default:
                }
            }
            if (date != null) {
                groundWater.setRecordDate(date);
                groundWater.setStCode(getStCode((String) dataMap.get("I_F1")));
                log.info("GroundWaterEntity:{}", groundWater.toString());
            }
        }
        return groundWater;
    }

    */
/**
     * 解析报文正文 获取土壤墒情信息对象
     *
     * @return
     *//*

    public SoilMoistureEntity soilMoistureData(Map<String, Object> dataMap) throws ParseException, ClassNotFoundException {
        SoilMoistureEntity soilMoisture = null;
        if (!dataMap.isEmpty()) {
            soilMoisture = new SoilMoistureEntity();
            Date date = null;
            for (String key : dataMap.keySet()) {
                switch (SL651IdentifierEnum.valueOf(key.length() % 2 == 0 ? key : key.substring(0, key.length() - 1))) {
                    case I_10:
                        soilMoisture.setSlm10(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_11:
                        soilMoisture.setSlm20(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_12:
                        soilMoisture.setSlm30(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_13:
                        soilMoisture.setSlm40(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_14:
                        soilMoisture.setSlm50(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_15:
                        soilMoisture.setSlm60(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_16:
                        soilMoisture.setSlm80(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_17:
                        soilMoisture.setSlm100(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    case I_18:
                        soilMoisture.setSurfaceMoistureContent(DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                        break;
                    default:
                }
            }
            if (date != null) {
                soilMoisture.setRecordDate(date);
                soilMoisture.setStCode(getStCode((String) dataMap.get("I_F1")));
                log.info("SoilMoistureEntity:{}", soilMoisture.toString());
            }
        }
        return soilMoisture;
    }

    */
/**
     * 解析报文正文 获取气温水温信息对象
     *//*

    public WaterAndAirTempEntity waterAndAirTempData(Map<String, Object> dataMap) throws ParseException, ClassNotFoundException {
        WaterAndAirTempEntity waterAndAirTemp = null;
        if (!dataMap.isEmpty()) {
            Date date = null;
            Double air = null;
            Double water = null;
            for (String key : dataMap.keySet()) {
                switch (SL651IdentifierEnum.valueOf(key.length() % 2 == 0 ? key : key.substring(0, key.length() - 1))) {
                    case I_02:
                        air = (DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        date = (Date) dataMap.get("I_F0" + key.substring(4));
                    case I_03:
                        water = (DataDecodeUtil.value((String) dataMap.get(key), SL651IdentifierEnum.valueOf(key.substring(0, 4))));
                        break;
                    default:
                }
            }
            if (date != null) {
                waterAndAirTemp = new WaterAndAirTempEntity();
                waterAndAirTemp.setAirTemp(air);
                waterAndAirTemp.setWaterTemp(water);
                waterAndAirTemp.setRecordDate(date);
                waterAndAirTemp.setStCode(getStCode((String) dataMap.get("I_F1")));
                log.info("WaterAndAirTempEntity:{}", waterAndAirTemp.toString());
            }
        }
        return waterAndAirTemp;
    }

    public static int getWindPower(Double windSpeed) {
        if (windSpeed >= 0.0 && windSpeed <= 0.2) {
            return 0;
        } else if (windSpeed >= 0.3 && windSpeed <= 1.5) {
            return 1;
        } else if (windSpeed >= 1.6 && windSpeed <= 3.3) {
            return 2;
        } else if (windSpeed >= 3.4 && windSpeed <= 5.4) {
            return 3;
        } else if (windSpeed >= 5.5 && windSpeed <= 7.9) {
            return 4;
        } else if (windSpeed >= 8.0 && windSpeed <= 10.7) {
            return 5;
        } else if (windSpeed >= 10.8 && windSpeed <= 13.8) {
            return 6;
        } else if (windSpeed >= 13.9 && windSpeed <= 17.1) {
            return 7;
        } else if (windSpeed >= 17.2 && windSpeed <= 20.7) {
            return 8;
        } else if (windSpeed >= 20.8 && windSpeed <= 24.4) {
            return 9;
        } else if (windSpeed >= 24.5 && windSpeed <= 28.4) {
            return 10;
        } else if (windSpeed >= 28.5 && windSpeed <= 32.6) {
            return 11;
        } else if (windSpeed >= 32.7 && windSpeed <= 36.9) {
            return 12;
        }
        return 0;
    }

    public ImagePkgEntity imgSolver(Map<String, Object> dataMap) throws ParseException, ClassNotFoundException {
        return new ImagePkgEntity().setStCode((String) dataMap.get(I_F1.getCode()))
                .setSerialNum((Integer) dataMap.get(I_E1.getCode()))
                .setPkgTotal((Integer) dataMap.get(I_E2.getCode()))
                .setPkgSerial((Integer) dataMap.get(I_E3.getCode()))
                .setValueHex((String) dataMap.get(I_F3.getCode()))
                .setRecordDate(DataDecodeUtil.value((String) dataMap.get(I_F0.getCode()), I_F0));

    }
}
*/
