package com.ruoyi.iot.axure.dto;

import lombok.Data;

import java.util.List;
@Data
public class DeviceDtoResult {

    /** 设备列表*/
    private List<DeviceDto> list;

    /** 设备总数量*/
    private Integer deviceNumber;

    /**设备在线总数量*/
    private Integer deviceOnlineNumber;

}
