package com.ruoyi.iot.axure.sl651;

/**
 * @Author 王嫣然
 * @since 2022/11/29
 */
public class SL651Constant {

    //控制字符编码（帧起始）
    public final static String CONTROL_CHAR_SOH = "7E7E";

    //控制字符编码（报文正文起始）
    public final static String CONTROL_CHAR_START_STX = "02";

    //控制字符编码（多包传输正文起始）
    public final static String CONTROL_CHAR_START_SYN = "16";

    //控制字符编码（报文结束,后续无报文）
    public final static String CONTROL_CHAR_END_ETX = "03";

    //控制字符编码（报文结束,后续有报文）
    public final static String CONTROL_CHAR_END_ETB = "17";
}
