package com.ruoyi.iot.axure.utils;

import java.io.*;
import java.net.Socket;
import java.util.List;

public class TCPUtil {
    private static String serverAddress = "112.30.214.13"; // 中心站的IP地址
    private static int serverPort = 6000; // 中心站的端口号

    //递归深度
    static int num = 0;

    //version 3.0(单独发送数据，需要传入发送数据的水文数据)
   /* public static void sendMessage(String resultJson) {
        //递归深度，到了一定的深度就结束方法，避免栈内存溢出
        num++;
        if (num >= 750) {
            num = 0;
            return;
        }
        Socket socket = null;
        OutputStream outputStream = null;
        InputStream inputStream = null;
        try {
            socket = new Socket(serverAddress, serverPort);
            //设置超时重试时间，超过这个时间就会尝试重新连接
            socket.setSoTimeout(1000);
            System.out.println("成功连接中心站。。。");
            outputStream = socket.getOutputStream();
            inputStream = socket.getInputStream();
            String waterDataHex = resultJson; // SL651-2014协议的水文报文的十六进制字符串
            byte[] dataBytes = hexStringToByteArray(waterDataHex);
            outputStream.write(dataBytes);
            outputStream.flush();
            // 获取输入流，用于接收中心站的确认响应
            byte[] buffer = new byte[1024];
            int length = inputStream.read(buffer);
            String response = new String(buffer, 0, length);
            //处理中心站的确认响应
            System.out.println("中心站的确认响应：" + response);
            // 关闭输入流、输出流和Socket连接
            //inputStream.close();
            //outputStream.close();
            //socket.close();
            //socket.shutdownOutput();
            //如果已经发送到中心站，修改num值为初始值
            num = 0;
            System.out.println("水文报文已发送至中心站");
        } catch (Exception e) {
            System.out.println("无法连接中心站：" + e.getMessage());
            //进行重试
            sendMessage(resultJson);
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }*/
    //version 3.1 (单独发送数据，需要传入水文数据以及中心站的ip和端口)
    public static void sendMessage(String resultJson, String ip, Integer port) {
        //递归深度，到了一定的深度就结束方法，避免栈内存溢出
        num++;
        if (num >= 750) {
            num = 0;
            return;
        }
        Socket socket = null;
        OutputStream outputStream = null;
        InputStream inputStream = null;
        try {
            socket = new Socket(ip, port);
            //设置超时重试时间，超过这个时间就会尝试重新连接
            socket.setSoTimeout(1000);
            //System.out.println("成功连接中心站。。。");
            outputStream = socket.getOutputStream();
            inputStream = socket.getInputStream();
            // SL651-2014协议的水文报文的十六进制字符串
            byte[] dataBytes = hexStringToByteArray(resultJson);
            outputStream.write(dataBytes);
            outputStream.flush();
            // 获取输入流，用于接收中心站的确认响应
            byte[] buffer = new byte[1024];
            int length = inputStream.read(buffer);
            String response = new String(buffer, 0, length);
            System.out.println("单独发送的报文为："+resultJson);
            //处理中心站的确认响应
            System.out.println("中心站的确认响应：" + response);
            //如果已经发送到中心站，修改num值为初始值
            num = 0;
            System.out.println("水文报文已发送至中心站");
        } catch (Exception e) {
            //System.out.println("无法连接中心站：" + e.getMessage());
            //进行重试
            sendMessage(resultJson, ip, port);
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //批量发送
    public static void sendMessages(List<String> resultJson) {
        Socket socket = null;
        OutputStream outputStream = null;
        InputStream inputStream = null;
        try {
            socket = new Socket(serverAddress, serverPort);
            System.out.println("成功连接中心站。。。");
            outputStream = socket.getOutputStream();
            inputStream = socket.getInputStream();
            for (String json : resultJson) {
                byte[] dataBytes = hexStringToByteArray(json);
                outputStream.write(dataBytes);
                outputStream.flush();
                System.out.println("批量发送的报文为：" + json);
            }
            // 获取输入流，用于接收中心站的确认响应
            //InputStream inputStream = socket.getInputStream();
            byte[] buffer = new byte[1024];
            int length = inputStream.read(buffer);
            String response = new String(buffer, 0, length);
            System.out.println("中心站的确认响应：" + response);
            // 关闭输入流、输出流和Socket连接
            //inputStream.close();
            //outputStream.close();
            //socket.shutdownOutput();
            //socket.close();
            System.out.println("水文报文已发送至中心站");
        } catch (Exception e) {
            System.out.println("无法连接中心站：" + e.getMessage());
            // 进行重试
            sendMessages(resultJson);
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // 将十六进制字符串转换为字节数组
    private static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
}
