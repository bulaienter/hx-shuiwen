/*
package com.ruoyi.iot.axure.sl651;


import cn.hutool.core.date.DateUtil;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Date;

*/
/**
 * @ClassName DataUnit
 * @Description 数据单元 代表协议里数据帧每个字段
 * @Author Zheng Xiaoyu
 * Date 2022/4/13 9:59
 * Version 1.0
 **//*

@Data
public class DataUnit implements Serializable {

    //字段名
    private String fieldName;

    //编码方式 HEX/ASCII
    private String encoding;

    //字段长度
    private int length;

    //常量值 特定字段有指定的值,比对值有效性
    private String constant = null;

    //字段值数据类型
    private String type;

    //浮点数据精度
    private String accuracy;

    //字段值
    private byte[] value;

    public String valueToHex() {
        if (length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            sb.append(String.format("%02x", value[i]));
        }
        return sb.toString().toUpperCase();
    }

    public String valueToASC() {
        if (length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append((char) value[i]);
        }
        return sb.toString();
    }

    public String valueToBCD() {
        if (length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            sb.append(String.format("%02d", value[i]));
        }
        return sb.toString();
    }

    public int valueToInt() {
        if ("Integer".equals(type) && length > 0) {
            if ("ASCII".equals(encoding)) {
                return Integer.parseInt(valueToASC());
            } else {
                return Integer.parseInt(valueToHex(), 16);
            }
        }
        return 0;
    }

    public double valueToDouble() {
        if ("Double".equals(type) && length > 0) {
            int acc = Integer.parseInt(accuracy);
            if ("ASCII".equals(encoding)) {
                BigDecimal dividend = new BigDecimal(Integer.parseInt(valueToASC()));
                BigDecimal divisor = new BigDecimal(Math.pow(10, acc));
                return dividend.divide(divisor, acc, BigDecimal.ROUND_HALF_UP).doubleValue();
            } else {
                BigDecimal dividend = new BigDecimal(Integer.parseInt(valueToHex()));
                BigDecimal divisor = new BigDecimal(Math.pow(10, acc));
                return dividend.divide(divisor, acc, BigDecimal.ROUND_HALF_UP).doubleValue();
            }
        }
        return 0;
    }

    public Date valueToDate() throws ParseException {
        if ("Date".equals(type)) {
            if ("ASCII".equals(encoding)) {
                return DateUtil.parse(valueToASC(), DateUtil.PATTERN_DIGIT);
            } else {
                String current = String.valueOf(DateUtil.getCurrentYear());
                String bcd = valueToBCD();
                return DateUtil.parse(current.substring(0, 2) + bcd, DateUtil.PATTERN_DIGIT);
            }
        }
        return null;
    }

}
*/
