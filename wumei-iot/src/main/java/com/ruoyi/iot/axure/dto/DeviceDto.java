package com.ruoyi.iot.axure.dto;

import lombok.Data;

@Data
public class DeviceDto {

    /** 设备名称 */
    private String deviceName;

    /** 设备编号 */
    private String serialNumber;

    /** 设备状态（1-未激活，2-禁用，3-在线，4-离线） */
    private Integer status;

    /**场所名称*/
    private String locationName;
}
