/*
package com.ruoyi.iot.axure.sl651;



import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

*/
/**
 * @Author 王嫣然
 * @since 2022/11/29
 *//*

@Data
@Component
@PropertySource(value = "classpath:SL651.yml", factory = YamlPropertyLoaderFactory.class)
@ConfigurationProperties(prefix = "sl651")
public class SL651Template {
    */
/**
     * 规约名称
     *//*

    private String protocol;

    */
/**
     * 报文帧
     *//*

    private DataFrame dataFrame;
}
*/
