/*
package com.ruoyi.iot.axure.sl651;

import cn.hutool.core.io.unit.DataUnit;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

*/
/**
 * @ClassName DataFrame
 * @Description 报文帧
 * @Author Zheng Xiaoyu
 * Date 2022/4/13 10:14
 * Version 1.0
 **//*

@Data
public class DataFrame implements Serializable {

    // 报文帧头,包括 起始标识+报文类型+数据长度
    private List<DataUnit> header;

    // 数据域
    private DataField dataField;

    // 报文帧尾
    private DataUnit tail;

    public String valueToHex() {
        StringBuilder sb = new StringBuilder();
        for (DataUnit dataUnit:header ) {
            sb.append(dataUnit.valueToHex());
        }
        sb.append(dataField.dataParamsMaptoHex());
        return sb.toString();
    }

}
*/
