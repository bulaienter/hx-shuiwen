package com.ruoyi.iot.axure.controller;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.iot.axure.dto.HydrologyDto;
import com.ruoyi.iot.axure.dto.DeviceAlertDtoResult;
import com.ruoyi.iot.axure.dto.DeviceDtoResult;
import com.ruoyi.iot.axure.domain.Result;
import com.ruoyi.iot.service.IAlertLogService;
import com.ruoyi.iot.service.IAlertService;
import com.ruoyi.iot.service.IDeviceService;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/iot/result")
public class ResultController {
    @Autowired
    private IDeviceService deviceService;

    @Autowired
    private IAlertService alertService;

    @Autowired
    private IDeviceService iDeviceService;

    @Autowired
    private IAlertLogService iAlertLogService;

    // 1.设备列表  =>完成
    @GetMapping("/getDeviceList")
    public AjaxResult getDeviceList() {
        DeviceDtoResult deviceDtoResult = deviceService.getDeviceList();
        AjaxResult result = AjaxResult.success(deviceDtoResult);
        return result;
    }

    // 2.设备预警&设备告警 =>开发完成（数据来源是在数据库添加的）
    @GetMapping("/getDeviceAlertList")
    public AjaxResult getDeviceAlertList() {
        DeviceAlertDtoResult deviceAlertDtoResult = iAlertLogService.getDeviceAlertList();
        return AjaxResult.success(deviceAlertDtoResult);
    }


    //3. 门禁/照明灯开关接口
    @GetMapping("/getSwitchList")
    public AjaxResult getSwitchList() {
        Result result = iDeviceService.getSwitchList();
        return AjaxResult.success(result);
    }

    //4、水库添加水位以及水库降雨量（前端页面修改数据）
    @PostMapping("/putHydrology")
    public AjaxResult putHydrology(@RequestBody HydrologyDto hydrology) {
        boolean b = deviceService.putHydrology(hydrology);
        if (b) {
            return AjaxResult.success("修改成功！");
        } else {
            return AjaxResult.error("修改失败！");
        }
    }

    //5、上报数据
    @PostMapping("/reportHydrology")
    public AjaxResult reportHydrology(@RequestBody HydrologyDto hydrology) {
        deviceService.reportHydrology(hydrology);
        return AjaxResult.success();
    }
}
