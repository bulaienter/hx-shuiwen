package com.ruoyi.iot.axure.sl651;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author 王嫣然
 * @since 2022/12/2
 */

@Getter
@AllArgsConstructor
public enum SL651IdentifierEnum {

    /**
     * 测站类型
     */
    I_E0("E0", 0, "测站类型", "", "java.lang.String", "HEX"),
    /**
     * 消息流水号
     */
    I_E1("E1", 0, "消息流水号", "", "java.lang.Integer", "HEX"),
    /**
     * 总包数
     */
    I_E2("E2", 0, "总包数", "", "java.lang.Integer", "HEX"),
    /**
     * 分包当前序列号
     */
    I_E3("E3", 0, "分包当前序列号", "", "java.lang.Integer", "HEX"),
    /**
     * 观测时间引导符a
     */
    I_F0("F0", 0, "观测时间引导符a", "", "java.util.Date", "BCD"),
    /**
     * 测站编码引导符b
     */
    I_F1("F1", 0, "测站编码引导符b", "", "java.lang.String", "BCD"),
    /**
     * 人工置数c
     */
    I_F2("F2", 0, "人工置数c", "d字节", "java.lang.String", "HEX"),
    /**
     * 图片信息d
     */
    I_F3("F3", 0, "图片信息d", "KB", "java.lang.String", "HEX"),
    /**
     * 1小时内每5分钟时段雨量
     */
    I_F4("F4", 1, "5", "0.1毫米", "java.lang.Double", "HEX"),
    /**
     * 1小时内5分钟间隔相对水位1
     */
    I_F5("F5", 2, "5", "0.01米", "java.lang.Double", "HEX"),
    /**
     * 1小时内5分钟间隔相对水位2
     */
    I_F6("F6", 2, "5", "0.01米", "java.lang.Double", "HEX"),
    /**
     * 1小时内5分钟间隔相对水位3
     */
    I_F7("F7", 2, "5", "0.01米", "java.lang.Double", "HEX"),
    /**
     * 1小时内5分钟间隔相对水位4
     */
    I_F8("F8", 2, "5", "0.01米", "java.lang.Double", "HEX"),
    /**
     * 1小时内5分钟间隔相对水位5
     */
    I_F9("F9", 2, "5", "0.01米", "java.lang.Double", "HEX"),
    /**
     * 1小时内5分钟间隔相对水位6
     */
    I_FA("FA", 2, "5", "0.01米", "java.lang.Double", "HEX"),
    /**
     * 1小时内5分钟间隔相对水位7
     */
    I_FB("FB", 2, "5", "0.01米", "java.lang.Double", "HEX"),
    /**
     * 1小时内5分钟间隔相对水位8
     */
    I_FC("FC", 2, "5", "0.01米", "java.lang.Double", "HEX"),
    /** 扩展标识符5分钟间隔相对水位 带基准值
     */
    I_FF07("FF07", 2, "5", "0.01米", "java.lang.Double", "HEX"),
    /**
     * 断面面积
     */
    I_01("01", 2, "断面面积", "平方米", "java.lang.Double", "BCD"),
    /**
     * 瞬时气温
     */
    I_02("02", 1, "瞬时气温", "摄氏度", "java.lang.Double", "BCD"),
    /**
     * 瞬时水温
     */
    I_03("03", 1, "瞬时水温", "摄氏度", "java.lang.Double", "BCD"),
    /**
     * 时间步长码
     */
    I_04("04", 0, "时间步长", "", "java.lang.Integer", "BCD"),
    /**
     * 时段长,降水、引排水、抽水历时
     */
    I_05("05", 0, "时段长,降水、引排水、抽水历时", "小时.分钟", "java.lang.Double", "BCD"),
    /**
     * 日蒸发量
     */
    I_06("06", 1, "日蒸发量", "毫米", "java.lang.Double", "BCD"),
    /**
     * 当前蒸发
     */
    I_07("07", 0, "当前蒸发", "毫米", "java.lang.Double", "BCD"),
    /**
     * 气压
     */
    I_08("08", 0, "气压", "百帕", "java.lang.Integer", "BCD"),
    /**
     * 闸坝、水库闸门开启高度
     */
    I_09("09", 2, "闸坝、水库闸门开启高度", "米", "java.lang.Double", "BCD"),
    /**
     * 输水设备、闸门(组)编号
     */
    I_0A("0A", 0, "输水设备、闸门(组)编号", "", "java.lang.Integer", "BCD"),
    /**
     * 输水设备类别
     */
    I_0B("0B", 0, "输水设备类别", "", "java.lang.Integer", "BCD"),
    /**
     * 水库、闸坝闸门开启孔数
     */
    I_0C("0C", 0, "水库、闸坝闸门开启孔数", "孔", "java.lang.Integer", "BCD"),
    /**
     * 地温
     */
    I_0D("0D", 1, "地温", "摄氏度", "java.lang.Double", "BCD"),
    /**
     * 地下水瞬时埋深
     */
    I_0E("0E", 2, "地下水瞬时埋深", "米", "java.lang.Double", "BCD"),
    /**
     * 波浪高度
     */
    I_0F("0F", 2, "波浪高度", "米", "java.lang.Double", "BCD"),
    /**
     * 10厘米处土壤含水量
     */
    I_10("10", 1, "10厘米处土壤含水量", "百分比", "java.lang.Double", "BCD"),
    /**
     * 20厘米处土壤含水量
     */
    I_11("11", 1, "20厘米处土壤含水量", "百分比", "java.lang.Double", "BCD"),
    /**
     * 30厘米处土壤含水量
     */
    I_12("12", 1, "30厘米处土壤含水量", "百分比", "java.lang.Double", "BCD"),
    /**
     * 40厘米处土壤含水量
     */
    I_13("13", 1, "40厘米处土壤含水量", "百分比", "java.lang.Double", "BCD"),
    /**
     * 50厘米处土壤含水量
     */
    I_14("14", 1, "50厘米处土壤含水量", "百分比", "java.lang.Double", "BCD"),
    /**
     * 60厘米处土壤含水量
     */
    I_15("15", 1, "60厘米处土壤含水量", "百分比", "java.lang.Double", "BCD"),
    /**
     * 80厘米处土壤含水量
     */
    I_16("16", 1, "80厘米处土壤含水量", "百分比", "java.lang.Double", "BCD"),
    /**
     * 100厘米处土壤含水量
     */
    I_17("17", 1, "100厘米处土壤含水量", "百分比", "java.lang.Double", "BCD"),
    /**
     * 湿度
     */
    I_18("18", 1, "湿度", "百分比", "java.lang.Double", "BCD"),
    /**
     * 开机台数
     */
    I_19("19", 0, "开机台数", "台", "java.lang.Integer", "BCD"),
    /**
     * 1小时时段降水量
     */
    I_1A("1A", 1, "60", "毫米", "java.lang.Double", "BCD"),
    /**
     * 2小时时段降水量
     */
    I_1B("1B", 1, "120", "毫米", "java.lang.Double", "BCD"),
    /**
     * 3小时时段降水量
     */
    I_1C("1C", 1, "180", "毫米", "java.lang.Double", "BCD"),
    /**
     * 6小时时段降水量
     */
    I_1D("1D", 1, "240", "毫米", "java.lang.Double", "BCD"),
    /**
     * 12小时时段降水量
     */
    I_1E("1E", 1, "720", "毫米", "java.lang.Double", "BCD"),
    /**
     * 日降水量
     */
    I_1F("1F", 1, "日降水量", "毫米", "java.lang.Double", "BCD"),
    /**
     * 当前降水量
     */
    I_20("20", 1, "1", "毫米", "java.lang.Double", "BCD"),
    /**
     * 1分钟时段降水量
     */
    I_21("21", 1, "1", "毫米", "java.lang.Double", "BCD"),
    /**
     * 5分钟时段降水量
     */
    I_22("22", 1, "5", "毫米", "java.lang.Double", "BCD"),
    /**
     * 10分钟时段降水量
     */
    I_23("23", 1, "10", "毫米", "java.lang.Double", "BCD"),
    /**
     * 30分钟时段降水量
     */
    I_24("24", 1, "30", "毫米", "java.lang.Double", "BCD"),
    /**
     * 暴雨量
     */
    I_25("25", 1, "暴雨量", "毫米", "java.lang.Double", "BCD"),
    /**
     * 降水量累计值
     */
    I_26("26", 1, "降水量累计值", "毫米", "java.lang.Double", "BCD"),
    /**
     * 瞬时流量、抽水流量
     */
    I_27("27", 3, "瞬时流量、抽水流量", "立方米/秒", "java.lang.Double", "BCD"),
    /**
     * 取(排）水口流量1
     */
    I_28("28", 3, "取(排）水口流量1", "立方米/秒", "java.lang.Double", "BCD"),
    /**
     * 取(排）水口流量2
     */
    I_29("29", 3, "取(排）水口流量2", "立方米/秒", "java.lang.Double", "BCD"),
    /**
     * 取(排）水口流量3
     */
    I_2A("2A", 3, "取(排）水口流量3", "立方米/秒", "java.lang.Double", "BCD"),
    /**
     * 取(排）水口流量4
     */
    I_2B("2B", 3, "取(排）水口流量4", "立方米/秒", "java.lang.Double", "BCD"),
    /**
     * 取(排）水口流量5
     */
    I_2C("2C", 3, "取(排）水口流量5", "立方米/秒", "java.lang.Double", "BCD"),
    /**
     * 取(排）水口流量6
     */
    I_2D("2D", 3, "取(排）水口流量6", "立方米/秒", "java.lang.Double", "BCD"),
    /**
     * 取(排）水口流量7
     */
    I_2E("2E", 3, "取(排）水口流量7", "立方米/秒", "java.lang.Double", "BCD"),
    /**
     * 取(排）水口流量8
     */
    I_2F("2F", 3, "取(排）水口流量8", "立方米/秒", "java.lang.Double", "BCD"),
    /**
     * 总出库流量、过闸总流量
     */
    I_30("30", 3, "总出库流量、过闸总流量", "立方米/秒", "java.lang.Double", "BCD"),
    /**
     * 输水设备流量、过闸(组)流量
     */
    I_31("31", 3, "输水设备流量、过闸(组)流量", "立方米/秒", "java.lang.Double", "BCD"),
    /**
     * 输沙量
     */
    I_32("32", 3, "输沙量", "万吨", "java.lang.Double", "BCD"),
    /**
     * 风向
     */
    I_33("33", 2, "风向", "", "java.lang.Integer", "BCD"),
    /**
     * 风力(级)
     */
    I_34("34", 2, "风力(级)", "", "java.lang.Integer", "BCD"),
    /**
     * 风速
     */
    I_35("35", 1, "风速", "米/秒", "java.lang.Double", "BCD"),
    /**
     * 断面平均流速
     */
    I_36("36", 3, "断面平均流速", "米/秒", "java.lang.Double", "BCD"),
    /**
     * 当前瞬时流速
     */
    I_37("37", 3, "当前瞬时流速", "米/秒", "java.lang.Double", "BCD"),
    /**
     * 电源电压
     */
    I_38("38", 2, "电源电压", "伏特", "java.lang.Double", "BCD"),
    /**
     * 瞬时河道水位、潮位
     */
    I_39("39", 3, "瞬时河道水位、潮位", "米", "java.lang.Double", "BCD"),
    /**
     * 库(闸、站)下水位
     */
    I_3A("3A", 3, "库(闸、站)下水位", "米", "java.lang.Double", "BCD"),
    /**
     * 库(闸、站)上水位
     */
    I_3B("3B", 3, "库(闸、站)上水位", "米", "java.lang.Double", "BCD"),
    /**
     * 取(排）水口水位1
     */
    I_3C("3C", 3, "取(排）水口水位1", "米", "java.lang.Double", "BCD"),
    /**
     * 取(排）水口水位2
     */
    I_3D("3D", 3, "取(排）水口水位2", "米", "java.lang.Double", "BCD"),
    /**
     * 取(排）水口水位3
     */
    I_3E("3E", 3, "取(排）水口水位3", "米", "java.lang.Double", "BCD"),
    /**
     * 取(排）水口水位4
     */
    I_3F("3F", 3, "取(排）水口水位4", "米", "java.lang.Double", "BCD"),
    /**
     * 取(排）水口水位5
     */
    I_40("40", 3, "取(排）水口水位5", "米", "java.lang.Double", "BCD"),
    /**
     * 取(排）水口水位6
     */
    I_41("41", 3, "取(排）水口水位6", "米", "java.lang.Double", "BCD"),
    /**
     * 取(排）水口水位7
     */
    I_42("42", 3, "取(排）水口水位7", "米", "java.lang.Double", "BCD"),
    /**
     * 取(排）水口水位8
     */
    I_43("43", 3, "取(排）水口水位8", "米", "java.lang.Double", "BCD"),
    /**
     * 含沙量
     */
    I_44("44", 3, "含沙量", "千克/立方米", "java.lang.Double", "BCD"),
    /**
     * 遥测站状态及报警信息（定义见表
     */
    I_45("45", 0, "遥测站状态及报警信息", "", "java.lang.Integer", "HEX"),
    /**
     * pH
     */
    I_46("46", 2, "pH", "值", "java.lang.Double", "BCD"),
    /**
     * 溶解氧
     */
    I_47("47", 1, "溶解氧", "毫克/升", "java.lang.Double", "BCD"),
    /**
     * 电导率
     */
    I_48("48", 0, "电导率", "微西门/厘米", "java.lang.Integer", "BCD"),
    /**
     * 浊度
     */
    I_49("49", 0, "浊度", "度", "java.lang.Integer", "BCD"),
    /**
     * 高锰酸盐指数
     */
    I_4A("4A", 1, "高锰酸盐指数", "毫克/升", "java.lang.Double", "BCD"),
    /**
     * 氧化还原电位
     */
    I_4B("4B", 1, "氧化还原电位", "毫伏", "java.lang.Double", "BCD"),
    /**
     * 氨氮
     */
    I_4C("4C", 2, "氨氮", "毫克/升", "java.lang.Double", "BCD"),
    /**
     * 总磷
     */
    I_4D("4D", 3, "总磷", "毫克/升", "java.lang.Double", "BCD"),
    /**
     * 总氮
     */
    I_4E("4E", 2, "总氮", "毫克/升", "java.lang.Double", "BCD"),
    /**
     * 总有机碳
     */
    I_4F("4F", 2, "总有机碳", "毫克/升", "java.lang.Double", "BCD"),
    /**
     * 铜
     */
    I_50("50", 4, "铜", "毫克/升", "java.lang.Double", "BCD"),
    /**
     * 锌
     */
    I_51("51", 4, "锌", "毫克/升", "java.lang.Double", "BCD"),
    /**
     * 硒
     */
    I_52("52", 5, "硒", "毫克/升", "java.lang.Double", "BCD"),
    /**
     * 砷
     */
    I_53("53", 5, "砷", "毫克/升", "java.lang.Double", "BCD"),
    /**
     * 总汞
     */
    I_54("54", 5, "总汞", "毫克/升", "java.lang.Double", "BCD"),
    /**
     * 镉
     */
    I_55("55", 5, "镉", "毫克/升", "java.lang.Double", "BCD"),
    /**
     * 铅
     */
    I_56("56", 5, "铅", "毫克/升", "java.lang.Double", "BCD"),
    /**
     * 叶绿素a
     */
    I_57("57", 2, "叶绿素a", "毫克/升", "java.lang.Double", "BCD"),
    /**
     * 水压1
     */
    I_58("58", 2, "水压1", "千帕", "java.lang.Double", "BCD"),
    /**
     * 水压2
     */
    I_59("59", 2, "水压2", "千帕", "java.lang.Double", "BCD"),
    /**
     * 水压3
     */
    I_5A("5A", 2, "水压3", "千帕", "java.lang.Double", "BCD"),
    /**
     * 水压4
     */
    I_5B("5B", 2, "水压4", "千帕", "java.lang.Double", "BCD"),
    /**
     * 水压5
     */
    I_5C("5C", 2, "水压5", "千帕", "java.lang.Double", "BCD"),
    /**
     * 水压6
     */
    I_5D("5D", 2, "水压6", "千帕", "java.lang.Double", "BCD"),
    /**
     * 水压7
     */
    I_5E("5E", 2, "水压7", "千帕", "java.lang.Double", "BCD"),
    /**
     * 水压8
     */
    I_5F("5F", 2, "水压8", "千帕", "java.lang.Double", "BCD"),
    /**
     * 水表1剩余水量
     */
    I_60("60", 3, "水表1剩余水量", "立方米", "java.lang.Double", "BCD"),
    /**
     * 水表2剩余水量
     */
    I_61("61", 3, "水表2剩余水量", "立方米", "java.lang.Double", "BCD"),
    /**
     * 水表3剩余水量
     */
    I_62("62", 3, "水表3剩余水量", "立方米", "java.lang.Double", "BCD"),
    /**
     * 水表
     */
    I_63("63", 3, "水表1剩余水量", "立方米", "java.lang.Double", "BCD"),
    /**
     * 水表5剩余水量
     */
    I_64("64", 3, "水表5剩余水量", "立方米", "java.lang.Double", "BCD"),
    /**
     * 水表6剩余水量
     */
    I_65("65", 3, "水表6剩余水量", "立方米", "java.lang.Double", "BCD"),
    /**
     * 水表7剩余水量
     */
    I_66("66", 3, "水表7剩余水量", "立方米", "java.lang.Double", "BCD"),
    /**
     * 水表8剩余水量
     */
    I_67("67", 3, "水表8剩余水量", "立方米", "java.lang.Double", "BCD"),
    /**
     * 水表1每小时水量
     */
    I_68("68", 2, "水表1每小时水量", "立方米/小时", "java.lang.Double", "BCD"),
    /**
     * 水表2每小时水量
     */
    I_69("69", 2, "水表2每小时水量", "立方米/小时", "java.lang.Double", "BCD"),
    /**
     * 水表3每小时水量
     */
    I_6A("6A", 2, "水表3每小时水量", "立方米/小时", "java.lang.Double", "BCD"),
    /**
     * 水表4每小时水量
     */
    I_6B("6B", 2, "水表4每小时水量", "立方米/小时", "java.lang.Double", "BCD"),
    /**
     * 水表5每小时水量
     */
    I_6C("6C", 2, "水表5每小时水量", "立方米/小时", "java.lang.Double", "BCD"),
    /**
     * 水表6每小时水量
     */
    I_6D("6D", 2, "水表6每小时水量", "立方米/小时", "java.lang.Double", "BCD"),
    /**
     * 水表7每小时水量
     */
    I_6E("6E", 2, "水表7每小时水量", "立方米/小时", "java.lang.Double", "BCD"),
    /**
     * 水表8每小时水量
     */
    I_6F("6F", 2, "水表8每小时水量", "立方米/小时", "java.lang.Double", "BCD"),
    /**
     * 交流A相电压
     */
    I_70("70", 1, "交流A相电压", "伏特", "java.lang.Double", "BCD"),
    /**
     * 交流B相电压
     */
    I_71("71", 1, "交流B相电压", "伏特", "java.lang.Double", "BCD"),
    /**
     * 交流C相电压
     */
    I_72("72", 1, "交流C相电压", "伏特", "java.lang.Double", "BCD"),
    /**
     * 交流A相电流
     */
    I_73("73", 1, "交流A相电流", "安培", "java.lang.Double", "BCD"),
    /**
     * 交流B相电流
     */
    I_74("74", 1, "交流B相电流", "安培", "java.lang.Double", "BCD"),
    /**
     * 交流C相电流
     */
    I_75("75", 1, "交流C相电流", "安培", "java.lang.Double", "BCD"),
    /**
     * 渗压计1
     */
    I_FF1b00("FF1b00", 0, "渗压计1", "m", "java.lang.Integer", "BCD"),
    /**
     * 渗压计2
     */
    I_FF1c00("FF1c00", 0, "渗压计2", "m", "java.lang.Integer", "BCD"),
    /**
     * 渗压计3
     */
    I_FF1d00("FF1d00", 1, "渗压计3", "m", "java.lang.Double", "BCD"),
    /**
     * 降水量------------
     *
     */
    I_FF4700("FF4700", 1, "降水量", "mm", "java.lang.Double", "BCD"),
    /**
     * 渗压检测编号标识
     */
    I_FF1100("FF1100", 2, "渗压检测编号标识", "4", "java.lang.Double", "BCD"),
    /**
     * 渗流检测编号标识
     */
    I_FF1200("FF1200", 3, "渗流检测编号标识", "4", "java.lang.Double", "BCD"),
    /**
     * 水平位移编号标识
     */
    I_FF1300("FF1300", 2, "水平位移编号标识", "4", "java.lang.Double", "BCD"),
    /**
     * 渗压检测数据标识
     */
    I_FF1400("FF1400", 2, "渗压检测数据标识", "5", "java.lang.Double", "BCD"),
    /**
     * 渗流检测数据标识
     */
    I_FF1500("FF1500", 4, "渗流检测数据标识", "5", "java.lang.Double", "BCD"),
    /**
     * 水平位移x数据标识
     */
    I_FF1600("FF1600", 4, "水平位移x数据标识", "5", "java.lang.Double", "BCD"),
    /**
     * 水平位移Y数据标识
     */
    I_FF1700("FF1700", 5, "水平位移Y数据标识", "5", "java.lang.Double", "BCD"),
    /**
     * 垂直位移数据标识
     */
    I_FF1800("FF1800", 5, "垂直位移数据标识", "5", "java.lang.Double", "BCD"),
    /**
     * 水平位移坐标标识
     */
    I_FF1900("FF1900", 5, "水平位移坐标标识", "8", "java.lang.Double", "BCD"),
    /**
     * 垂直位移坐标标识
     */
    I_FF2000("FF2000", 5, "垂直位移坐标标识", "7", "java.lang.Double", "BCD"),
    /**
     * 垂直位移高程标识
     */
    I_FF2100("FF2100", 5, "垂直位移高程标识", "5", "java.lang.Double", "BCD"),
    /**
     * 通道1信号强度标识符
     */
    I_F104("F104", 2, "通道1信号强度标识符", "毫克/升", "java.lang.Double", "BCD"),
    /**
     * 通道2信号强度标识符
     */
    I_F105("F105", 2, "通道2信号强度标识符", "千帕", "java.lang.Double", "BCD");
    /**
     * 表示符号
     */
    private String code;

    /**
     * 精度
     */
    private Integer accuracy;
    /**
     * 描述
     */
    private String description;

    /**
     * 量和单位
     */
    private String unit;

    /**
     * 数据类型
     */
    private String type;

    /**
     * 编码方式
     */
    private String encoding;


    }
