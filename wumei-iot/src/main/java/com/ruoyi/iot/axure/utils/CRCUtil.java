package com.ruoyi.iot.axure.utils;

/**
 * 将HEX 字符串生成CRC校验码工具类
 */
public class CRCUtil {
    public static String calculateCRC(String hexString) {
        int crc = 0xFFFF; // 初始值为FFFF

        for (int i = 0; i < hexString.length(); i += 2) {
            int data = Integer.parseInt(hexString.substring(i, i + 2), 16);

            crc = crc ^ data;
            for (int j = 0; j < 8; j++) {
                if ((crc & 0x0001) == 1) {
                    crc = (crc >> 1) ^ 0xA001; // 多项式8005
                } else {
                    crc = crc >> 1;
                }
            }
        }

        // 将计算结果高位在左，低位在右，转换为字符串
        return String.format("%04X", crc);
    }
}