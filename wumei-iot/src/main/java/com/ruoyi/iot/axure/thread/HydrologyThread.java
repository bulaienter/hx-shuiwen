package com.ruoyi.iot.axure.thread;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class HydrologyThread implements Runnable {
    private String message;
    private int num = 0;

    public HydrologyThread(String message) {
        this.message = message;
    }

    //线程任务就是给指定的Ip发送报文
    @Override
    public void run() {
        //TCPUtil.sendMessage(message);
        sendMessage(message);
    }

    public void sendMessage(String message) {
        //递归深度，到了一定的深度就结束方法，避免栈内存溢出
        num++;
        if (num >= 750) {
            //num = 0;
            return;
        }
        Socket socket = null;
        OutputStream outputStream = null;
        InputStream inputStream = null;
        try {
            socket = new Socket("112.30.214.13", 6000);
            //设置超时重试时间，超过这个时间就会尝试重新连接
            socket.setSoTimeout(1000);
            System.out.println("成功连接中心站。。。");
            outputStream = socket.getOutputStream();
            inputStream = socket.getInputStream();
            // SL651-2014协议的水文报文的十六进制字符串
            byte[] dataBytes = hexStringToByteArray(message);
            outputStream.write(dataBytes);
            outputStream.flush();
            // 获取输入流，用于接收中心站的确认响应
            byte[] buffer = new byte[1024];
            int length = inputStream.read(buffer);
            String response = new String(buffer, 0, length);
            //处理中心站的确认响应
            System.out.println("中心站的确认响应：" + response);
            // 关闭输入流、输出流和Socket连接
            //inputStream.close();
            //outputStream.close();
            //socket.close();
            //socket.shutdownOutput();
            //如果已经发送到中心站，修改num值为初始值
            //num = 0;
            System.out.println("水文报文已发送至中心站");
        } catch (Exception e) {
            System.out.println("无法连接中心站：" + e.getMessage());
            //进行重试
            sendMessage(message);
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // 将十六进制字符串转换为字节数组
    private byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
}
