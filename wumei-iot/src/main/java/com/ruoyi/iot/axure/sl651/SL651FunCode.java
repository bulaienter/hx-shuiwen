package com.ruoyi.iot.axure.sl651;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author 王嫣然
 * @since 2022/12/2
 */
@Getter
@AllArgsConstructor
public enum SL651FunCode {

    F_2F("2F","","链路保持报",""),
    F_30("30","","测试报",""),
    F_31("31","","均匀时段水文信息",""),
    F_32("32","","遥测站定时报",""),
    F_33("33","","遥测站加报报",""),
    F_34("34","","遥测站小时报",""),
    F_35("35","","遥测站人工置数报",""),
    F_36("36","","遥测站图片报或中心站查询遥测站图片采集信息","");
    /**
     * 表示符号
     */
    private String code;

    /**
     * ASCII码
     */
    private String ascii;
    /**
     * 描述
     */
    private String description;

    /**
     * 量和单位
     */
    private String unit;

}
