package com.ruoyi.iot.axure.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 设备告警
 */
@Data
public class DeviceAlertDto {
    /**
     * 报警位置
     */
    private String alertLocation;

    /**
     * 告警时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createTime;
    /**
     * 告警名称
     */
    private String alertName;

    /**
     * 告警级别（1=提醒通知，2=轻微问题，3=严重警告）
     */
    private Long alertLevel;

    /**
     * 告警时间格式化字符串响应前端
     */
    private String alertTime;
}
