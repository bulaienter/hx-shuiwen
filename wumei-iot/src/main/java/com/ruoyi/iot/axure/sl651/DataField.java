/*
package com.ruoyi.iot.axure.sl651;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

*/
/**
 * @ClassName DataField
 * @Description 数据域
 * @Author Zheng Xiaoyu
 * Date 2022/4/13 10:21
 * Version 1.0
 **//*

@Data
public class DataField implements Serializable {

    private DataUnit dataHeader;

    private HashMap<String, List<DataUnit>> dataParamsMap;

    private DataUnit crcCode;

    private DataUnit dataTail;

    public String dataParamsMaptoHex() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry : dataParamsMap.entrySet()) {
            List<DataUnit> list = (List<DataUnit>) entry.getValue();
            for (DataUnit dataUnit : list) {
                if (dataUnit.getLength() > 0 && dataUnit.getValue() != null) {
                    sb.append(dataUnit.valueToHex());
                }
            }
        }
        sb.append(dataTail.valueToHex()).append(crcCode.valueToHex());
        return sb.toString();
    }

    public String dataParamsMaptoASC() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry : dataParamsMap.entrySet()) {
            List<DataUnit> list = (List<DataUnit>) entry.getValue();
            for (DataUnit dataUnit : list) {
                if (dataUnit.getLength() > 0 && dataUnit.getValue() != null) {
                    sb.append(dataUnit.valueToASC());
                }
            }
        }
        return sb.toString();
    }

}
*/
