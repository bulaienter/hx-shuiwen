package com.ruoyi.iot.axure.sl651;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author 王嫣然
 * @since 2022/12/2
 */
@Getter
@AllArgsConstructor
public enum SL651StTypeEnum {

    /**
     * 降水站 降水|蒸发|气象
     */
    S_50("50", "P"),

    /**
     * 河道站 降水|蒸发|河道水情|气象|水质
     */
    S_48("48", "H"),

    /**
     * 水库（湖泊）站 降水|蒸发|水库水情|气象|水质
     */
    S_4B("4B", "K"),

    /**
     * 闸坝站 降水|蒸发|闸坝水情|气象|水质
     */
    S_5A("5A", "Z"),

    /**
     * 泵站 降水|蒸发|泵站水情|气象|水质
     */
    S_44("44", "D"),

    /**
     * 潮汐站 降水|蒸发|潮汐水情|气象
     */
    S_54("54", "T"),

    /**
     * 墒情站 降水|蒸发|墒情
     */
    S_4D("4D", "M"),

    /**
     * 地下水站 埋深|水质|开采量
     */
    S_47("47", "G"),

    /**
     * 水质站 水质|流量|水位
     */
    S_51("51", "Q"),

    /**
     * 取水口站 水位|水质|水量|水压等
     */
    S_49("49", "I"),

    /**
     * 排水口站 水位|水质|水量|水压等
     */
    S_4F("4F", "O");

    /**
     * 测站分类码HEX
     */
    private String code;

    /**
     * ASCII码
     */
    private String ascii;

}
