package com.ruoyi.iot.axure.sl651;

import lombok.Data;

/**
 * @ClassName Identifer
 * @Description SL651协议 要素标识符类
 * @Author Zheng Xiaoyu
 * Date 2022/12/2 10:39
 * Version 1.0
 **/
@Data
public class SL651Identifer {

    /**
     * 标识符 hex字符串
     */
    private String code;

    /**
     * 数据长度
     */
    private int length;

    /**
     * 数据长度
     */
    private int accuracy;

}
