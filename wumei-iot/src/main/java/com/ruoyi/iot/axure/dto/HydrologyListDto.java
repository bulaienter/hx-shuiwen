package com.ruoyi.iot.axure.dto;

import lombok.Data;
/**查询水库列表及其点位信息*/
@Data
public class HydrologyListDto {

    //水库id
    private Long deviceId;

    //水库编码
    private String serialNumber;

    //水库名称
    private String deviceName;

    //水库水位
    private Double waterLevel;

    //水库降雨量
    private Double rainfall;
}
