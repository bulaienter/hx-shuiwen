package com.ruoyi.iot.axure.dto;

import com.ruoyi.iot.axure.domain.DeviceAlertDto;
import lombok.Data;

import java.util.List;

@Data
public class DeviceAlertDtoResult {
    /**
     * 严重报警数量
     */
    private Integer criticalAlarmNum;

    /**
     * 一般报警数量
     */
    private Integer generalAlarmNum;

    /**
     * 报警设备列表
     */
    private List<DeviceAlertDto> alertDtoList;
}
