package com.ruoyi.iot.axure.domain;

import lombok.Data;

@Data
public class Result {
    private String type;
    private String messageContent;
}
