package com.ruoyi.iot.axure.thread;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class HydrologyNewThread implements Runnable {
    //发送的水文
    private String message;
    //中心站IP
    private String addressIp;
    //中心站端口
    private Integer addressPort;
    private int num = 0;

    public HydrologyNewThread(String addressIp, Integer addressPort, String message) {
        this.addressIp = addressIp;
        this.addressPort = addressPort;
        this.message = message;
    }

    //线程任务就是给指定的中心站发送报文
    @Override
    public void run() {
        sendMessage(addressIp, addressPort, message);
    }

    public void sendMessage(String addressIp, Integer addressPort, String message) {
        //递归深度，到了一定的深度就结束方法，避免栈内存溢出
        num++;
        if (num >= 850) {
            return;
        }
        Socket socket = null;
        OutputStream outputStream = null;
        InputStream inputStream = null;
        try {
            socket = new Socket(addressIp, addressPort);
            socket.setSoTimeout(1000);
            //System.out.println("成功连接中心站。。。");
            outputStream = socket.getOutputStream();
            inputStream = socket.getInputStream();
            byte[] dataBytes = hexStringToByteArray(message);
            outputStream.write(dataBytes);
            outputStream.flush();
            //接收中心站的确认响应
            byte[] buffer = new byte[1024];
            int length = inputStream.read(buffer);
            String response = new String(buffer, 0, length);
            System.out.println("定时发送的报文为：" + message);
            System.out.println("中心站的确认响应：" + response);
            System.out.println("水文报文已发送至中心站");
        } catch (Exception e) {
            //System.out.println("无法连接中心站：" + e.getMessage());
            //进行重试
            sendMessage(addressIp, addressPort, message);
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // 将十六进制字符串转换为字节数组
    private byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
}
