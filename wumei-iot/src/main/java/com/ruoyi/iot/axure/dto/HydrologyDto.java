package com.ruoyi.iot.axure.dto;

import lombok.Data;
/**添加水库水位及降雨量*/
@Data
public class HydrologyDto {
    //水库id
    private Long deviceId;

    //水库编号
    private String serialNumber;

    //水库编码
    private String hydrologyNum;

    //水库水位
    private Double waterLevel;

    //水库降雨量
    private Double rainfall;

    //水库上报Ip
    private String addressIp;

    //水库上报端口号
    private Integer addressPort;
}
